<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from flps_numbernew.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="flps_numbernew" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>flps_numbernew</refname><refpurpose>Returns a new floating point number.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   flpn = flps_numbernew ( )
   flpn = flps_numbernew ( "integral" , flps , M , e )
   flpn = flps_numbernew ( "signm" , flps , s , m , e )
   flpn = flps_numbernew ( "double" , flps , x )
   flpn = flps_numbernew ( "hex" , flps , hexstr )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>flps :</term>
      <listitem><para> a floating point system</para></listitem></varlistentry>
   <varlistentry><term>rtype :</term>
      <listitem><para> a 1x1 matrix of strings, the type of representation, rtype="integral", rtype="signm", rtype="double"</para></listitem></varlistentry>
   <varlistentry><term>M :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the integral significand</para></listitem></varlistentry>
   <varlistentry><term>e :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the exponent</para></listitem></varlistentry>
   <varlistentry><term>s :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the sign</para></listitem></varlistentry>
   <varlistentry><term>m :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the normal significand</para></listitem></varlistentry>
   <varlistentry><term>x :</term>
      <listitem><para> a 1x1 matrix of doubles, the floating point number</para></listitem></varlistentry>
   <varlistentry><term>hexstr :</term>
      <listitem><para> a 1x1 matrix of strings, with length 16.</para></listitem></varlistentry>
   <varlistentry><term>flpn:</term>
      <listitem><para> a floating point number</para></listitem></varlistentry>
   <varlistentry><term>flpn.sign:</term>
      <listitem><para> 1x1 matrix of floating point integers, 0 if x is positive, 1 if x is negative</para></listitem></varlistentry>
   <varlistentry><term>flpn.M :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the integral significant, so that x = M * b^(e-p+1)</para></listitem></varlistentry>
   <varlistentry><term>flpn.m :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the normal significant, so that x = (-1)^s * m * b^e and 0 &lt;= m &lt; beta</para></listitem></varlistentry>
   <varlistentry><term>flpn.e :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the exponent</para></listitem></varlistentry>
   <varlistentry><term>flpn.flps :</term>
      <listitem><para> the floating point system supporting the number</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
The "flpn = flps_numbernew ( )" creates an empty floating point number.
   </para>
   <para>
If the "integral" representation is chosen,
returns a new floating point number with given (M,e) representation.
The sign s is computed by the rule s=0 is for x &gt;= 0,
s=1 is for x &lt; 0.
The normal significant is computed by the rule m = |M| * radix^(1-p).
No negative zero can be produced with this "integral" constructor.
   </para>
   <para>
Returns a new floating point number with given (s,m,e) representation.
The integral significant is computed by the rule M = (-1)^s * m * radix^(p - 1).
   </para>
   <para>
When rtype="double" and x=%nan, we set the sign bit to 1, the exponent bits to 1 and the
significand bits to 1.
This corresponds to a IEEE quiet nan.
We do not have any mean in Scilab to distinguish
a quiet nan and a signaling nan.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Empty constructor
flpn = flps_numbernew ( )

// A toy system
radix = 2;
p = 3;
ebits = 3;
flps = flps_systemnew ( "format" , radix , p , ebits );
flpn = flps_numbernew ( "integral" , flps , 4 , 2 )
string(flpn)
flpn = flps_numbernew ( "signm" , flps , 0 , 1 , 2 )
string(flpn)

// Create numbers from actual doubles
flps = flps_systemnew ( "IEEEsingle" );
flpn = flps_numbernew ( "double" , flps , 1/3 )
flpn = flps_numbernew ( "double" , flps , 3*2^-128 )
// Check rounding modes : default mode is round to nearest
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )
// A case of tie: check that round to even is applied
x= 4.5 * 2^(-1-3+1);
flpn = flps_numbernew ( "double" , flps , x )
// A case of tie: check that round to even is applied
x= 4.5 * 2^(0-3+1);
flpn = flps_numbernew ( "double" , flps , x )
// A case of tie: check that round to even is applied
x= -4.5 * 2^(0-3+1);
flpn = flps_numbernew ( "double" , flps , x )
// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , 0.49 )
// Check rounding modes : set rounding mode to round up
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 2;
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )
// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , 0.99 )
// Check rounding modes : set rounding mode to round down
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 3;
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )
// A case where the number is rounded up, which makes the exponent decrease
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , -0.49 )
// Check rounding modes : set rounding mode to round to zero
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 4;
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )

// Create numbers from doubles
flps = flps_systemnew ( "IEEEdouble" );
flpn = flps_numbernew ( "double" , flps , +0 )
flpn = flps_numbernew ( "double" , flps , -0 )
flpn = flps_numbernew ( "double" , flps , +1 )
flpn = flps_numbernew ( "double" , flps , -1 )
flpn = flps_numbernew ( "double" , flps , +%inf )
flpn = flps_numbernew ( "double" , flps , -%inf )
flpn = flps_numbernew ( "double" , flps , %nan )
flpn = flps_numbernew ( "double" , flps , +2-%eps )
flpn = flps_numbernew ( "double" , flps , -2+%eps )
flpn = flps_numbernew ( "double" , flps , flps.vmin )
flpn = flps_numbernew ( "double" , flps , flps.vmax )
flpn = flps_numbernew ( "double" , flps , flps.alpha )
flpn = flps_numbernew ( "double" , flps , flps.vmin*(1-flps.eps) )
flpn = flps_numbernew ( "double" , flps , flps.vmin*(1-2*flps.eps) )

// Create numbers in integral form
flps = flps_systemnew("IEEEdouble");
// Create +Infinity
flps_numbernew ( "integral" , flps , +0 , flps.emax + 1 )
// Create -Infinity
flps_numbernew ( "integral" , flps , -0 , flps.emax + 1 )
// Create Nan
flps_numbernew ( "integral" , flps , +1 , flps.emax + 1 )
// Create +0
flps_numbernew ( "integral" , flps , +0 , flps.emin )
// Create -0
flps_numbernew ( "integral" , flps , -0 , flps.emin )
// Create Largest positive subnormal
flps_numbernew ( "integral" , flps , flps.radix^(flps.p-1)-1 , flps.emin )
// Create Largest negative subnormal
flps_numbernew ( "integral" , flps , -(flps.radix^(flps.p-1)-1) , flps.emin )
// Create Smallest positive subnormal
flps_numbernew ( "integral" , flps , +1 , flps.emin )
// Create Smallest negative subnormal
flps_numbernew ( "integral" , flps , -1 , flps.emin )

// Create a number (a double) from an hex
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "hex" , flps , "400921FB54442D18" )

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin, 2008 - 2010</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010</para>
   <para>http://en.wikipedia.org/wiki/Double_precision</para>
</refsection>
</refentry>
