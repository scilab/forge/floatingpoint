<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
-->
<refentry
  xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:db="http://docbook.org/ns/docbook"
  version="5.0-subset Scilab"
  xml:lang="fr"
  xml:id="floatingpoint_IEEEstandard">

  <refnamediv>
    <refname>IEEE standard</refname>

    <refpurpose>IEEE standard</refpurpose>
  </refnamediv>

  <refsection>
    <title>Purpose</title>

    <para>
    In this page, we present the part of the IEEE standard relevant to the
    management of single (called binary32 in the standard) and double precision
    (called binary64 in the standard) binary floating point numbers.
    We do not cover here the binary128 format and the two decimal decimal64 and
    decimal128 formats.
    </para>
  </refsection>

  <refsection>
    <title>The standard</title>

    <para>
    The IEEE 754 2008 formats are defined depending on their radix, their precision p, their maximum
    exponent emax, their minimum exponent emin and the number of bits in the exponent w.
    The two IEEE 754 2008 binary formats that we consider are the following.
    <itemizedlist>
    <listitem>single precision (binary32): radix=2, p = 24, emax=127, emin=-126, w=8.</listitem>
    <listitem>double precision (binary64): radix=2, p = 53, emax=1023, emin=-1022, w=11.</listitem>
    </itemizedlist>
    These two formats are detailed in the following sessions, where the number of bits in the
    exponents is denoted by ebits (instead of the letter w used in the standard).
    </para>

   <programlisting role="example"><![CDATA[
-->flps = flps_systemnew("IEEEsingle")
 flps  =

Floating Point System:
======================
radix= 2
p=     24
emin=  -126
emax=  127
vmin=  1.175D-38
vmax=  3.403D+38
eps=   0.0000001
r=     1
gu=    T
alpha= 1.401D-45
ebits= 8
   ]]></programlisting>

   <programlisting role="example"><![CDATA[
-->flps = flps_systemnew("IEEEdouble")
 flps  =
Floating Point System:
======================
radix= 2
p=     53
emin=  -1022
emax=  1023
vmin=  2.22D-308
vmax=  1.79D+308
eps=   2.220D-16
r=     1
gu=    T
alpha= 4.94D-324
ebits= 11
   ]]></programlisting>

    <para>
    In the IEEE 754 2008 standard, there is an encoding which allows to store
    both normal numbers and particular numbers such as infinities, NaN, subnormal numbers and
    signed zeros.
    In the following, we denote by E the biased exponent, defined by
    </para>

    <para>
     <latex>
     \begin{eqnarray}
     E = e + \textrm{bias}
     \end{eqnarray}
     </latex>
    </para>

    <para>
    where the bias is equal to the maximum exponent:
    </para>

    <para>
     <latex>
     \begin{eqnarray}
     \textrm{bias} = e_{max}
     \end{eqnarray}
     </latex>
    </para>

    <para>
    Depending on the biased exponent E, the normal significand m and the sign s, we
    can know if a number belong to a particular class of numbers.
    <itemizedlist>
    <listitem>1 &#8804; E &#8804; 2^w-2: normal number. This corresponds to emin &#8804; e &#8804; emax.</listitem>
    <listitem>E=2^w-1 and m &#8800; 0 : NaN (whatever the sign s). This corresponds to e = emax + 1.</listitem>
    <listitem>E=2^w-1 and m = 0 : +Infinity or -Infinity (depending on the sign s). This corresponds to e = emax + 1.</listitem>
    <listitem>E=0 and m &#8800; 0 : subnormal number. This corresponds to e = emin - 1.</listitem>
    <listitem>E=0 and m = 0 : signed zero. This corresponds to e = emin - 1.</listitem>
    </itemizedlist>
    </para>

    <para>
    In Scilab, we cannot know if a particular double x, for which isnan(x) is true,
    is a signaling NaN or a quiet Nan.
    </para>

  </refsection>

  <refsection>
    <title>Subnormal numbers</title>

    <para>
    In this section, we analyse the particularities of subnormal numbers, which
    are more difficult to manage.
    </para>

    <para>
    The minimum exponent of a IEEE 754 double precision number is emin=-1022.
    Now consider the smallest positive subnormal number, which is approximately 4.94D-324.
    This number is associated with a mathematical integral significant equal to M=1
    and an exponent e=-1022.
    In the following session, we compute the floating point representation of this number.
    </para>

   <programlisting role="example"><![CDATA[
-->flpn = flps_numbernew ( "double" , flps , (1 * 2^(-53+1)) * 2^-1022 )
 flpn  =
Floating Point Number:
======================
s= 0
M= 1
m= 2.220D-16
e= -1022
flps= floating point system
======================
Other representations:
x= (-1)^0 * 2.220D-16 * 2^-1022
x= 1 * 2^(-1022-53+1)
Sign= 0
Exponent= 00000000000
Significand= 0000000000000000000000000000000000000000000000000001
Hex= 0000000000000001
   ]]></programlisting>

    <para>
    The norma significant m is smaller than 1, which indicates that
    it is a subnormal number.
    Notice that the binary exponent is zero, which, according to the IEEE standard
    indeed corresponds to a subnormal number.
    The biased exponent is zero in this case and the implicit leading bit of the integral
    significant is zero.
    </para>

    <para>
    By contrast, let us see the floating point representation of the smallest normal
    floating point number, that is approximately 2.22D-308.
    </para>

   <programlisting role="example"><![CDATA[
-->flpn = flps_numbernew ( "double" , flps , flps.vmin )
 flpn  =
Floating Point Number:
======================
s= 0
M= 4.504D+15
m= 1
e= -1022
flps= floating point system
======================
Other representations:
x= (-1)^0 * 1 * 2^-1022
x= 4.504D+15 * 2^(-1022-53+1)
Sign= 0
Exponent= 00000000001
Significand= 0000000000000000000000000000000000000000000000000000
Hex= 0010000000000000
   ]]></programlisting>

    <para>
    We see that the value of the exponent is still e=-1022, but the
    normal significant m is equal to 1, which indicates that this is
    a normal floating point number.
    Hence, the binary exponent now ends with the "1" digit, so that the number
    is encoded with a biased exponent equal to 1, as required by the IEEE standard.
    In this case, the implicit leading bit of the integral significant is 1.
    </para>

  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>2010 - DIGITEO - Michael Baudin</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>
      "Handbook of Floating-Point Arithmetic", Muller, Brisebarre, de
      Dinechin, Jeannerod, Lefevre, Melquiond, Revol, Stehle, Torres, Birkhauser Boston, 2010
    </para>

    <para>
      "IEEE Standard for Floating-Point Arithmetic", IEEE Computer Society,
      IEEE Std 754-2008, 29 August 2008
    </para>

  </refsection>

</refentry>
