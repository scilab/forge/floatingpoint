//
// This help file was automatically generated from flps_numbernew.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_numbernew.sci
//

// Empty constructor
flpn = flps_numbernew ( )
halt()   // Press return to continue

// A toy system
radix = 2;
p = 3;
ebits = 3;
flps = flps_systemnew ( "format" , radix , p , ebits );
flpn = flps_numbernew ( "integral" , flps , 4 , 2 )
string(flpn)
flpn = flps_numbernew ( "signm" , flps , 0 , 1 , 2 )
string(flpn)
halt()   // Press return to continue

// Create numbers from actual doubles
flps = flps_systemnew ( "IEEEsingle" );
flpn = flps_numbernew ( "double" , flps , 1/3 )
flpn = flps_numbernew ( "double" , flps , 3*2^-128 )
// Check rounding modes : default mode is round to nearest
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )
// A case of tie: check that round to even is applied
x= 4.5 * 2^(-1-3+1);
flpn = flps_numbernew ( "double" , flps , x )
// A case of tie: check that round to even is applied
x= 4.5 * 2^(0-3+1);
flpn = flps_numbernew ( "double" , flps , x )
// A case of tie: check that round to even is applied
x= -4.5 * 2^(0-3+1);
flpn = flps_numbernew ( "double" , flps , x )
// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , 0.49 )
// Check rounding modes : set rounding mode to round up
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 2;
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )
// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , 0.99 )
// Check rounding modes : set rounding mode to round down
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 3;
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )
// A case where the number is rounded up, which makes the exponent decrease
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , -0.49 )
// Check rounding modes : set rounding mode to round to zero
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 4;
flpn = flps_numbernew ( "double" , flps , 0.54 )
flpn = flps_numbernew ( "double" , flps , 0.60 )
flpn = flps_numbernew ( "double" , flps , -0.54 )
flpn = flps_numbernew ( "double" , flps , -0.60 )
halt()   // Press return to continue

// Create numbers from doubles
flps = flps_systemnew ( "IEEEdouble" );
flpn = flps_numbernew ( "double" , flps , +0 )
flpn = flps_numbernew ( "double" , flps , -0 )
flpn = flps_numbernew ( "double" , flps , +1 )
flpn = flps_numbernew ( "double" , flps , -1 )
flpn = flps_numbernew ( "double" , flps , +%inf )
flpn = flps_numbernew ( "double" , flps , -%inf )
flpn = flps_numbernew ( "double" , flps , %nan )
flpn = flps_numbernew ( "double" , flps , +2-%eps )
flpn = flps_numbernew ( "double" , flps , -2+%eps )
flpn = flps_numbernew ( "double" , flps , flps.vmin )
flpn = flps_numbernew ( "double" , flps , flps.vmax )
flpn = flps_numbernew ( "double" , flps , flps.alpha )
flpn = flps_numbernew ( "double" , flps , flps.vmin*(1-flps.eps) )
flpn = flps_numbernew ( "double" , flps , flps.vmin*(1-2*flps.eps) )
halt()   // Press return to continue

// Create numbers in integral form
flps = flps_systemnew("IEEEdouble");
// Create +Infinity
flps_numbernew ( "integral" , flps , +0 , flps.emax + 1 )
// Create -Infinity
flps_numbernew ( "integral" , flps , -0 , flps.emax + 1 )
// Create Nan
flps_numbernew ( "integral" , flps , +1 , flps.emax + 1 )
// Create +0
flps_numbernew ( "integral" , flps , +0 , flps.emin )
// Create -0
flps_numbernew ( "integral" , flps , -0 , flps.emin )
// Create Largest positive subnormal
flps_numbernew ( "integral" , flps , flps.radix^(flps.p-1)-1 , flps.emin )
// Create Largest negative subnormal
flps_numbernew ( "integral" , flps , -(flps.radix^(flps.p-1)-1) , flps.emin )
// Create Smallest positive subnormal
flps_numbernew ( "integral" , flps , +1 , flps.emin )
// Create Smallest negative subnormal
flps_numbernew ( "integral" , flps , -1 , flps.emin )
halt()   // Press return to continue

// Create a number (a double) from an hex
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "hex" , flps , "400921FB54442D18" )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_numbernew.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
