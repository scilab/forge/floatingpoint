// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("floatingpoint.dem.gateway.sce");
subdemolist = [
"flps_tobary", "flps_tobary.sce"; ..
"flps_systemnew", "flps_systemnew.sce"; ..
"flps_systemgui", "flps_systemgui.sce"; ..
"flps_systemall", "flps_systemall.sce"; ..
"flps_sme2M", "flps_sme2M.sce"; ..
"flps_signbit", "flps_signbit.sce"; ..
"flps_radix", "flps_radix.sce"; ..
"flps_numbernew", "flps_numbernew.sce"; ..
"flps_numberiszero", "flps_numberiszero.sce"; ..
"flps_numberissubnormal", "flps_numberissubnormal.sce"; ..
"flps_numberisnormal", "flps_numberisnormal.sce"; ..
"flps_numberisnan", "flps_numberisnan.sce"; ..
"flps_numberisinf", "flps_numberisinf.sce"; ..
"flps_numberisfinite", "flps_numberisfinite.sce"; ..
"flps_numbergetclass", "flps_numbergetclass.sce"; ..
"flps_numberformat", "flps_numberformat.sce"; ..
"flps_numbereval", "flps_numbereval.sce"; ..
"flps_number2hex", "flps_number2hex.sce"; ..
"flps_minimumdecimalstr", "flps_minimumdecimalstr.sce"; ..
"flps_Me2sm", "flps_Me2sm.sce"; ..
"flps_isIEEE", "flps_isIEEE.sce"; ..
"flps_hex2double", "flps_hex2double.sce"; ..
"flps_frombary", "flps_frombary.sce"; ..
"flps_frexp", "flps_frexp.sce"; ..
"flps_eps", "flps_eps.sce"; ..
"flps_emin", "flps_emin.sce"; ..
"flps_emax", "flps_emax.sce"; ..
"flps_double2hex", "flps_double2hex.sce"; ..
"flps_chop", "flps_chop.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
