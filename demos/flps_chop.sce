//
// This help file was automatically generated from flps_chop.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_chop.sci
//

format("v",25)
// See 1/3 before choping
1/3
c = flps_chop ( 1/3 )
c = flps_chop ( [1/3 0 0 1 2 1/5] )
c = flps_chop ( [1/3 0 0 1 2 1/5] , 48 )
c = flps_chop ( 0.123456789 , 3 , 10 )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_chop.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
