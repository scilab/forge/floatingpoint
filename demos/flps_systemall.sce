//
// This help file was automatically generated from flps_systemall.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_systemall.sci
//

radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e );
// There are 56 f.p. numbers
listflpn = flps_systemall ( flps );
x = flps_numbereval ( listflpn )'
// Ignore denormals
listflpn = flps_systemall ( flps , %f );
x = flps_numbereval ( listflpn )'
// Get only positive numbers, ignore denormals
listflpn = flps_systemall ( flps , %f , %t );
x = flps_numbereval ( listflpn )'
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_systemall.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
