//
// This help file was automatically generated from flps_numbergetclass.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_numbergetclass.sci
//

flps = flps_systemnew ( "IEEEdouble" )
flpn = flps_numbernew ( "double" , flps , +0 )
class = flps_numbergetclass ( flpn ) // 5
flpn = flps_numbernew ( "double" , flps , -0 )
class = flps_numbergetclass ( flpn ) // 5
flpn = flps_numbernew ( "double" , flps , -%inf );
class = flps_numbergetclass ( flpn ) // 2
flpn = flps_numbernew ( "double" , flps , %inf );
class = flps_numbergetclass ( flpn ) // 2
flpn = flps_numbernew ( "double" , flps , %nan );
class = flps_numbergetclass ( flpn ) // 1
flpn = flps_numbernew ( "double" , flps , 1 );
class = flps_numbergetclass ( flpn ) // 3
flpn = flps_numbernew ( "double" , flps , -1 );
class = flps_numbergetclass ( flpn ) // 3
flpn = flps_numbernew ( "double" , flps , 5.e-320 )
class = flps_numbergetclass ( flpn ) // 4
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_numbergetclass.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
