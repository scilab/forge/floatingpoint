//
// This help file was automatically generated from flps_systemnew.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_systemnew.sci
//

// Create an empty floating point system
flps = flps_systemnew ()
flps.radix = 2
flps.p = 12
// Convert the floating point system into a string
string(flps)
halt()   // Press return to continue

// Create an empty floating point system
flps = flps_systemnew ( "empty" )
halt()   // Press return to continue

// Compute the current floating point properties
flps = flps_systemnew ( "current" )
halt()   // Press return to continue

// IEEE single
flps = flps_systemnew ( "IEEEsingle" )
// ... is the same as ...
flps = flps_systemnew ( "format" , 2 , 24 , 8 )
halt()   // Press return to continue

// IEEE double
flps = flps_systemnew ( "IEEEdouble" )
// ... is the same as ...
flps = flps_systemnew ( "format" , 2 , 53 , 1 )
halt()   // Press return to continue

// IEEE double extended
flps = flps_systemnew ( "IEEEdoubleext" )
// ... is the same as ...
flps = flps_systemnew ( "format" , 2 , 64 , 15 )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_systemnew.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
