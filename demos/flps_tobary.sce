//
// This help file was automatically generated from flps_tobary.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_tobary.sci
//

// This is d = [1 0]
d = flps_tobary ( 1 , 2 , 2 )
// This is d = [0 1 0 1]
x = 1/3+1/3^3
d = flps_tobary ( x , 3 , 4 )
// This is d = [1 1 0 1 0 1]
x = 1 + 1/2 + 1/2^3 + 1/2^5
d = flps_tobary ( x , 2 , 6 )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_tobary.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
