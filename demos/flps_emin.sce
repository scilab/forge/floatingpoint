//
// This help file was automatically generated from flps_emin.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_emin.sci
//

[radix,rounding] = flps_radix ();
[eps,p] = flps_eps (radix);
[ emin , vmin , gu , alpha ] = flps_emin ( radix , p )
number_properties( "tiny" ) // This is emin
number_properties( "tiniest" ) // This is alpha
number_properties( "minexp" )
number_properties( "denorm" )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_emin.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
