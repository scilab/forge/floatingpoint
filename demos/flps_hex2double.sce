//
// This help file was automatically generated from flps_hex2double.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_hex2double.sci
//

expected = %pi;
x = flps_hex2double ("400921FB54442D18")
halt()   // Press return to continue

halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_hex2double.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
