//
// This help file was automatically generated from flps_emax.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_emax.sci
//

[radix,rounding] = flps_radix ();
[eps,p] = flps_eps (radix);
[ emax , vmax ] = flps_emax ( radix , p )
number_properties( "huge" ) // Compare with vmax
number_properties( "maxexp" ) // Compare with emax
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_emax.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
