//
// This help file was automatically generated from flps_isIEEE.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_isIEEE.sci
//

// The following should return %t
isieee = flps_isIEEE ( )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_isIEEE.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
