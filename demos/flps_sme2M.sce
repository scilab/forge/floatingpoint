//
// This help file was automatically generated from flps_sme2M.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_sme2M.sci
//

radix = 2
p = 53
s= 0
m= 1.9999999999999997779554
e= 0
expected= 9007199254740991
M = flps_sme2M ( radix , p , s , m , e )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_sme2M.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
