//
// This help file was automatically generated from flps_frexp.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_frexp.sci
//

[ f , e ] = flps_frexp ( 1 , 2 ) // f=0.5, e=1
[ f , e ] = flps_frexp ( 1 , 3 ) // f=1/3, e=1
[ f , e ] = flps_frexp ( 1 , 3 , "v1" ) // f=1/3, e=1
[ f , e ] = flps_frexp ( 1 , 3 , "v2" ) // f=1, e=0
halt()   // Press return to continue

// With matrix input
b = [3 3 3 3];
x = [1 2 3 4];
[ f , e ] = flps_frexp ( x , b )
halt()   // Press return to continue

// With x scalar and b matrix
b = [2 3;5 7];
x = 2;
[ f , e ] = flps_frexp ( x , b )
halt()   // Press return to continue

// With b scalar and x matrix
x = [2 3;5 7];
b = 2;
[ f , e ] = flps_frexp ( x , b )
halt()   // Press return to continue

// Check IEEE values
x = [+0 -0 %inf -%inf %nan -%nan];
expected_f = [+0 -0 %inf -%inf %nan -%nan];
expected_e = [0   0 0    0     0    0];
[ f , e ] = flps_frexp ( x );
halt()   // Press return to continue

// Check extreme values
largestnormal = 9007199254740991 * 2^(1023-53+1);
[ f , e ] = flps_frexp ( largestnormal ) // f=1-%eps/2, e=1024
// The expression 2^e overflows
f*2^e
// This expression recovers x
(f*2) * (2^(e-1))
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_frexp.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
