//
// This help file was automatically generated from flps_numberiszero.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_numberiszero.sci
//

flps = flps_systemnew ( "IEEEdouble" );
// Get a zero
flpn = flps_numbernew ( "double" , flps , +0 );
is = flps_numberiszero ( flpn ) // %t
flpn = flps_numbernew ( "double" , flps , -0 );
is = flps_numberiszero ( flpn ) // %t
halt()   // Press return to continue

// Get others
flpn = flps_numbernew ( "double" , flps , -%inf );
is = flps_numberiszero ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , %inf );
is = flps_numberiszero ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , %nan );
is = flps_numberiszero ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , 1 );
is = flps_numberiszero ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , -1 );
is = flps_numberiszero ( flpn ) // %f
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_numberiszero.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
