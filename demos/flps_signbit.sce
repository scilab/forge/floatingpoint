//
// This help file was automatically generated from flps_signbit.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_signbit.sci
//

b = flps_signbit ( 1 ) // %f
b = flps_signbit ( -1 ) // %t
b = flps_signbit ( 0 ) // %f
b = flps_signbit ( -0 ) // %t
halt()   // Press return to continue

halt()   // Press return to continue

x =        [+0 -0 %inf -%inf %nan -%nan];
expected = [%f %t %f   %t    %f   %f]
b = flps_signbit ( x )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_signbit.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
