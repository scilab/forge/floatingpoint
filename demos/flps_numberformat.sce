//
// This help file was automatically generated from flps_numberformat.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_numberformat.sci
//

flps = flps_systemnew ( "IEEEsingle" );
flpn = flps_numberformat ( flps , 1/3 )
flpn = flps_numberformat ( flps , 3*2^-128 )
// Check rounding modes : default mode is round to nearest
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flpn = flps_numberformat ( flps , 0.54 )
flpn = flps_numberformat ( flps , 0.60 )
flpn = flps_numberformat ( flps , -0.54 )
flpn = flps_numberformat ( flps , -0.60 )
// A case of tie: check that round to even is applied
x= 4.5 * 2^(-1-3+1);
flpn = flps_numberformat ( flps , x )
// A case of tie: check that round to even is applied
x= 4.5 * 2^(0-3+1);
flpn = flps_numberformat ( flps , x )
// A case of tie: check that round to even is applied
x= -4.5 * 2^(0-3+1);
flpn = flps_numberformat ( flps , x )
// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numberformat ( flps , 0.49 )
// Check rounding modes : set rounding mode to round up
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 2;
flpn = flps_numberformat ( flps , 0.54 )
flpn = flps_numberformat ( flps , 0.60 )
flpn = flps_numberformat ( flps , -0.54 )
flpn = flps_numberformat ( flps , -0.60 )
// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numberformat ( flps , 0.99 )
// Check rounding modes : set rounding mode to round down
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 3;
flpn = flps_numberformat ( flps , 0.54 )
flpn = flps_numberformat ( flps , 0.60 )
flpn = flps_numberformat ( flps , -0.54 )
flpn = flps_numberformat ( flps , -0.60 )
// A case where the number is rounded up, which makes the exponent decrease
// to keep the number normalized
flpn = flps_numberformat ( flps , -0.49 )
// Check rounding modes : set rounding mode to round to zero
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 4;
flpn = flps_numberformat ( flps , 0.54 )
flpn = flps_numberformat ( flps , 0.60 )
flpn = flps_numberformat ( flps , -0.54 )
flpn = flps_numberformat ( flps , -0.60 )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_numberformat.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
