//
// This help file was automatically generated from flps_Me2sm.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_Me2sm.sci
//

radix = 2
p = 53
M= 9007199254740991
e= 0
expected_s= 0
expected_m= 1.9999999999999997779554
[s,m] = flps_Me2sm ( radix , p , M , e )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_Me2sm.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
