//
// This help file was automatically generated from flps_numberisnormal.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_numberisnormal.sci
//

flps = flps_systemnew ( "IEEEdouble" )
// Get normal
flpn = flps_numbernew ( "double" , flps , 1 );
is = flps_numberisnormal ( flpn ) // %t
flpn = flps_numbernew ( "double" , flps , -1 );
is = flps_numberisnormal ( flpn ) // %t
halt()   // Press return to continue

// Get others
flpn = flps_numbernew ( "double" , flps , %nan );
is = flps_numberisnormal ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , +0 );
is = flps_numberisnormal ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , -0 );
is = flps_numberisnormal ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , 5.e-320 )
is = flps_numberisnormal ( flpn ) // %f
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_numberisnormal.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
