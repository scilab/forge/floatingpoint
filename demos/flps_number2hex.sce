//
// This help file was automatically generated from flps_number2hex.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_number2hex.sci
//

expected = "400921FB54442D18";
flps = flps_systemnew ( "IEEEdouble" );
flpn = flps_numbernew ( "double" , flps , %pi );
hexstr = flps_number2hex (flpn)
halt()   // Press return to continue

// Get and extract the binary string
flps = flps_systemnew ( "IEEEdouble" );
ebits = flps.ebits;
p = flps.p;
flpn = flps_numbernew ( "double" , flps , %pi )
[hexstr,binstr] = flps_number2hex (flpn)
sign_str = part(binstr,1) // sign_str="0"
expo_str = part(binstr,2:ebits+1) // expo_str="10000000000"
M_str = part(binstr,ebits+2:ebits+p) // M_str="1001001000011111101101010100010001000010110100011000"
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_number2hex.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
