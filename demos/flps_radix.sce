//
// This help file was automatically generated from flps_radix.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_radix.sci
//

[ radix , rounding ] = flps_radix ()
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_radix.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
