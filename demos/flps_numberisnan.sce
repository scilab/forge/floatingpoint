//
// This help file was automatically generated from flps_numberisnan.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_numberisnan.sci
//

flps = flps_systemnew ( "IEEEdouble" );
// Get a nan
flpn = flps_numbernew ( "double" , flps , %nan );
is = flps_numberisnan ( flpn ) // %t
halt()   // Press return to continue

// Get others
flpn = flps_numbernew ( "double" , flps , %inf );
is = flps_numberisnan ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , -%inf );
is = flps_numberisnan ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , +0 );
is = flps_numberisnan ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , -0 );
is = flps_numberisnan ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , 1 );
is = flps_numberisnan ( flpn ) // %f
flpn = flps_numbernew ( "double" , flps , -1 );
is = flps_numberisnan ( flpn ) // %f
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_numberisnan.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
