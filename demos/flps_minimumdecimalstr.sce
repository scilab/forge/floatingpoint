//
// This help file was automatically generated from flps_minimumdecimalstr.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_minimumdecimalstr.sci
//

format("v",25)
//
x = 1.1 // Displays 1.1000000000000000888178
xstr = string(x) // Displays "1.1000000000000000888178"
ystr = flps_minimumdecimalstr ( xstr ) // Displays "1.1"
y = evstr(ystr) // Displays 1.1000000000000000888178
//
flps_minimumdecimalstr ( "1.1000000000000000888178e-010" ) // "1.10000000000000008e-10"
flps_minimumdecimalstr ( "1.1000000000000000888178e+010" ) // "1.1e-10"
flps_minimumdecimalstr ( "0000000.00000001" ) // ".00000001"
flps_minimumdecimalstr ( "2.00047180001120725D-320" ) // "2.0004D-320"
flps_minimumdecimalstr ( "000000.0000000" ) // "0."
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_minimumdecimalstr.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
