// Copyright (C) 2010 - Michael Baudin

mode(-1);
lines(0);

function floatingpointBuildToolbox()
    // Uncomment to make a Debug version
    //setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES")

    TOOLBOX_NAME = "floatingpoint";
    TOOLBOX_TITLE = "Floatingpoint";

    toolbox_dir = get_absolute_file_path("builder.sce");

    tbx_builder_macros(toolbox_dir);
    tbx_builder_help(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
endfunction

floatingpointBuildToolbox();
clear floatingpointBuildToolbox;

