// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [s,m] = flps_Me2sm ( radix , p , M , e )
  //   Returns the (s,m) representation given (M,e).
  //
  // Calling Sequence
  //   [s,m] = flps_Me2sm ( radix , p , M , e )
  //
  // Parameters
  //   r : a 1x1 matrix of floating point integers, the radix
  //   p : a 1x1 matrix of floating point integers, the precision
  //   M : a 1x1 matrix of floating point integers, the integral significand
  //   e : a 1x1 matrix of floating point integers, the exponent
  //   s : a 1x1 matrix of floating point integers, the sign
  //   m : a 1x1 matrix of floating point integers, the normal significand
  //
  // Description
  //   Returns the sign s and the normal significant m given
  //   the (M,e) floating point representation.
  //   The formula is
  //
  //   <latex>
  //   \begin{eqnarray}
  //   m = |M| \beta^{1-p},
  //   \end{eqnarray}
  //   </latex>
  //
  //   where beta is the radix.
  //
  // Examples
  // radix = 2
  // p = 53
  // M= 9007199254740991
  // e= 0
  // expected_s= 0
  // expected_m= 1.9999999999999997779554
  // [s,m] = flps_Me2sm ( radix , p , M , e )
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_Me2sm" , rhs , 4 )
  apifun_checklhs ( "flps_Me2sm" , lhs , 1:2 )
  //
  apifun_checktype ( "flps_Me2sm" , radix , "radix" , 1 , "constant" )
  apifun_checktype ( "flps_Me2sm" , p , "p" , 2 , "constant" )
  apifun_checktype ( "flps_Me2sm" , M , "M" , 3 , "constant" )
  apifun_checktype ( "flps_Me2sm" , e , "e" , 4 , "constant" )
  //
  apifun_checkscalar ( "flps_Me2sm" , radix , "radix" , 1 )
  apifun_checkscalar ( "flps_Me2sm" , p ,     "p" ,     2 )
  apifun_checkscalar ( "flps_Me2sm" , M ,     "M" ,     3 )
  apifun_checkscalar ( "flps_Me2sm" , e ,     "e" ,     4 )
  //
  apifun_checkgreq ( "flps_Me2sm" , radix , "radix" , 1 , 0 )
  apifun_checkgreq ( "flps_Me2sm" , p ,     "p" ,     2 , 0 )
  //
  if ( flps_signbit(M) ) then
    s = 1
  else
    s = 0
  end
  m = abs(M) * radix^(1-p)
endfunction

