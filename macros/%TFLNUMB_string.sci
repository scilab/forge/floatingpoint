// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// %TFLNUMB_string --
//   Returns the string containing the floating point number.
//
function str = %TFLNUMB_string ( flpn )
  //
  str = []
  k = 1
  str(k) = sprintf("Floating Point Number:")
  k = k + 1
  str(k) = sprintf("======================")
  k = k + 1
  flps = flpn.flps;
  str(k) = sprintf("s= %s", _tostring(flpn.s))
  k = k + 1
  str(k) = sprintf("M= %s", _tostring(flpn.M))
  k = k + 1
  str(k) = sprintf("m= %s", _tostring(flpn.m))
  k = k + 1
  str(k) = sprintf("e= %s", _tostring(flpn.e))
  k = k + 1
  str(k) = sprintf("flps= floating point system")
  k = k + 1
  if ( flpn.M == [] ) then
    // The structure is not complete, do not continue.
    return
  end
  str(k) = sprintf("======================")
  k = k + 1
  str(k) = sprintf("Other representations:")
  if ( flps_numberisnan(flpn) ) then
    k = k + 1
    str(k) = sprintf("x= NaN")
  elseif ( flps_numberisinf(flpn) ) then
    if ( flpn.s==1 ) then
      k = k + 1
      str(k) = sprintf("x= -Infinity")
    else
      k = k + 1
      str(k) = sprintf("x= Infinity")
    end
  else
    k = k + 1
    str(k) = sprintf("x= (-1)^%s * %s * %s^%s", ..
    _tostring(flpn.s) , _tostring(flpn.m) , _tostring(flps.radix) , _tostring(flpn.e) )
    k = k + 1
    str(k) = sprintf("x= %s * %s^(%s-%s+1)", ..
    _tostring(flpn.M) , _tostring(flps.radix) , _tostring(flpn.e) , _tostring(flps.p) )
  end
  k = k + 1
  //
  // Binary, Hexa
  ebits = flps.ebits
  p = flps.p
  //
  if ( number_isdivisor(16,ebits+p) ) then
    [hexstr,binstr] = flps_number2hex(flpn);
    //
    str(k) = sprintf("Sign= %s", part(binstr,1))
    k = k + 1
    //
    str(k) = sprintf("Exponent= %s", part(binstr,2:ebits+1))
    k = k + 1
    str(k) = sprintf("Significand= %s", part(binstr,ebits+2:ebits+p))
    k = k + 1
    str(k) = sprintf("Hex= %s", hexstr)
    k = k + 1
  end
endfunction

function bool = _mlist_isfield ( s , fieldname )
  // Get the matrix of integers representing defined fields
  df = definedfields ( s )
  // Search for the index ifield associated with given fieldname
  ifield = find(s(1)==fieldname)
  // Search for ifield in the matrix of defined fields
  jj = find(df==ifield)
  bool = jj <> []
endfunction
function s = _tostring ( x )
  if ( x==[] ) then
    s = "[]"
  else
    n = size ( x , "*" )
    if ( n == 1 ) then
      s = string(x)
    else
      s = "["+strcat(string(x)," ")+"]"
    end
  end
endfunction



