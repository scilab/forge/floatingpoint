// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ emin , vmin , gu , alpha ] = flps_emin ( radix , p )
  //   Returns the minimum exponent and value before underflow.
  //
  // Calling Sequence
  //   [ emin , vmin , gu , alpha ] = flps_emin ( radix , p )
  //
  // Parameters
  //   radix: a 1x1 matrix of floating point integers, the radix
  //   p : a 1x1 matrix of floating point integers, the precision (number of digits in the significant)
  //   emin : a 1x1 matrix of doubles, minimum exponent before underflow
  //   vmin : a 1x1 matrix of doubles, minimum value before underflow
  //   gu : a 1x1 matrix of booleans, true if the current machine supports graceful underflow, as recommended by IEEE
  //   alpha : a 1x1 matrix of doubles, smallest positive subnormal number
  //
  // Description
  //   This algorithm includes the refinements introduced by the Lapack DLAMCH routine.
  //
  // Examples
  // [radix,rounding] = flps_radix ();
  // [eps,p] = flps_eps (radix);
  // [ emin , vmin , gu , alpha ] = flps_emin ( radix , p )
  // number_properties( "tiny" ) // This is emin
  // number_properties( "tiniest" ) // This is alpha
  // number_properties( "minexp" )
  // number_properties( "denorm" )
  //
  // Authors
  //   Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Algorithms to Reveal Properties of Floating-Point Arithmetic", Michael A. Malcolm, Stanford University, Communications of the ACM, Volume 15,  Issue 11 (November 1972), Pages: 949 - 951
  //   "More on Algorithms that Reveal Properties of Floating Point Arithmetic Units", W. Morven Gentleman, University of Waterloo, Scott B. Marovich, Purdue University, Communications of the ACM, Volume 17,  Issue 5 (May 1974), Pages: 276 - 277
  //   "ALGORITHM 665 - MACHAR: A Subroutine to Dynamically Determine Machine Parameters", W. J. Cody, Argonne National Laboratory, ACM Transactions on Mathematical Software, Vol. 14, No. 4, December 1988, Pages 303-311.
  //   DLAMCH, Lapack auxiliary routine , Univ. of Tennessee, Oak Ridge National Lab, Argonne National Lab, Courant Institute, Nag Ltd., and Rice University, March 26, 1990

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_emin" , rhs , 2 )
  apifun_checklhs ( "flps_emin" , lhs , 1:4 )
  //
  apifun_checktype ( "flps_emin" , radix , "radix" , 1 , "constant" )
  apifun_checktype ( "flps_emin" , p ,     "p" ,     2 , "constant" )
  //
  apifun_checkscalar ( "flps_emin" , radix , "radix" , 1 )
  apifun_checkscalar ( "flps_emin" , p ,     "p" ,     2 )
  //
  apifun_checkgreq ( "flps_emin" , radix , "radix" , 1 , 0 )
  apifun_checkgreq ( "flps_emin" , p ,     "p" ,     2 , 0 )
  //
  small = 1
  for i = 1 : 3
    small = small / radix
  end
  a = 1 + small
  // a is close to 1.125
  ngpmin = dlamc4 (  1 , radix )
  ngnmin = dlamc4 (  -1 , radix )
  gpmin = dlamc4 (  a , radix )
  gnmin = dlamc4 (  -a , radix )
  // Set gu to %t in case of gradual underflow
  gu = %f
  // Set iwarn to %t in case of an unmanaged case
  iwarn = %f
  if ( ( ngpmin==ngnmin ) & ( gpmin==gnmin ) ) then
    if ( ngpmin==gpmin ) then
      emin = ngpmin
      //  ( non twos-complement machines, no gradual underflow;s e.g.,  vax )
    elseif ( ( gpmin-ngpmin )==3 ) then
      emin = ngpmin - 1 + p
      gu = %t
      //  ( non twos-complement machines, with gradual underflow; e.g., ieee standard followers )
    else
      emin = min( [ ngpmin gpmin] )
      //  ( a guess; no known machine )
      iwarn = %t
    end
  elseif ( ( ngpmin==gpmin ) & ( ngnmin==gnmin ) ) then
    if ( abs( ngpmin-ngnmin )==1 ) then
      emin = max( [ ngpmin ngnmin ] )
      //  ( twos-complement machines, no gradual underflow; e.g., cyber 205 )
    else
      emin = min ( [ ngpmin ngnmin ] )
      //  ( a guess; no known machine )
      iwarn = %t
    end
  elseif ( ( abs( ngpmin-ngnmin )==1 ) & ( gpmin==gnmin ) ) then
    if ( ( gpmin-min ( [ ngpmin ngnmin] ) )==3 ) then
      emin = max ( [ ngpmin ngnmin ] ) - 1 + p
      gu = %t
      //  ( twos-complement machines with gradual underflow; no known machine )
    else
      emin = min ( [ ngpmin ngnmin ] )
      //  ( a guess; no known machine )
      iwarn = %t
    end
  else
    emin = min ( [ ngpmin ngnmin gpmin gnmin ] )
    //  ( a guess; no known machine )
    iwarn = %t
  end
  if ( iwarn ) then
    errmsg = msprintf(gettext("%s: Unexpected machine parameters."), "flps_emin");
    warning(errmsg)
  end
  //
  // Compute  vmin by successive division by  BETA. We could compute
  // vmin as radix**( EMIN - 1 ),  but some machines underflow during
  // this computation.
  //
  vmin = 1
  for i = 1 : 1 - emin
    vmin = vmin  / radix
  end
  //
  // Our definition is
  //    x = M * beta ^ ( e - p + 1 )
  // where M is the integral significant in the interval [0,beta^p - 1[
  // and e is the exponent in the interval emin <= e <= emax.
  // Because of this definition, we must decrease emin by 1.
  // This does not change vmin.
  //
  emin = emin - 1
  //
  // Compute alpha
  //
  alpha = vmin
  if ( gu ) then
    for i = 1 : p - 1
      alpha = alpha  / radix
    end
  end
endfunction


function emin = dlamc4 ( start , radix )
  //
  // Service routine for dlamc2.
  // Returns the number of iterations required to detect
  // underflow by starting with a = start.
  // The algorithm starts with a = start, and reduces a until
  // underflows occurs. Uses several methods to detect underflow:
  // Method #1 : c1 = (a/radix)*radix <> a
  // Method #2 : d1 = (a*rradix)/rradix <> a, where rradix = 1/radix
  // Method #3 : c2 = a/radix + a/radix + ... + a/radix <> a, where the sum is performed radix times
  // Method #4 : d2 = a*rradix + a*rradix + ... + a*rradix <> a, where the sum is performed radix times
  //
  a = start
  rradix = 1 / radix
  emin = 1
  b1 = a * rradix
  c1 = a
  c2 = a
  d1 = a
  d2 = a
  while ( %t )
    if ( ( c1 <> a ) | ( c2 <> a ) | ( d1 <> a ) | ( d2 <> a ) ) then
      break
    end
    emin = emin - 1
    a = b1
    b1 = a/radix
    c1 = b1*radix
    d1 = 0
    for i = 1 : radix
      d1 = d1 + b1
    end
    b2 = a*rradix
    c2 = b2/rradix
    d2 = 0
    for i = 1 : radix
      d2 = d2 + b2
    end
  end
endfunction

// A rather primitive version of the same algorithm.

function [ emin , vmin ] = flps_emin_basic ( radix , p )
  vmin = 1.
  emin = 1
  while ( %t )
    emin = emin - 1
    // Condition #1 : no exception is generated
    n = vmin / radix
    // Condition #2 : one can recover the original number
    if ( n * radix <> vmin) then
      break
    end
    vmin = n
  end
  // See in DMALCH.f, DLAMC2 relative to IEEE machines.
  // TODO : what happens on non-IEEE machine ?
  emin = emin + p
  vmin = vmin * radix^(p-1)
endfunction

