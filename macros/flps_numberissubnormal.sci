// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function is = flps_numberissubnormal ( flpn )
  // Returns true if the number is subnormal.
  //
  // Calling Sequence
  //   is = flps_numberissubnormal ( flpn )
  //
  // Parameters
  //   flpn : a floating point number
  //   is : a 1x1 matrix of boolean
  //
  // Description
  //   Returns true if the number is a subnormal.
  //   A subnormal has a minimum exponent to its maximum value and a nonzero integral
  //   significant.
  //   The implicit leading significand bit is then zero.
  //
  // Examples
  // flps = flps_systemnew ( "IEEEdouble" );
  // // Get a subnormal
  // flpn = flps_numbernew ( "double" , flps , 5.e-320 );
  // is = flps_numberissubnormal ( flpn ) // %t
  //
  // // Get others
  // flpn = flps_numbernew ( "double" , flps , %nan );
  // is = flps_numberissubnormal ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , +0 );
  // is = flps_numberissubnormal ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , -0 );
  // is = flps_numberissubnormal ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , 1 );
  // is = flps_numberissubnormal ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , -1 );
  // is = flps_numberissubnormal ( flpn ) // %f
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  is = ( flpn.e == flpn.flps.emin & abs(flpn.M) > 0 & abs(flpn.m) < 1 )
endfunction

