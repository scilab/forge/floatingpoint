// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function flps = flps_systemnew ( varargin )
  //   Returns a new floating point system.
  //
  // Calling Sequence
  // flps = flps_systemnew ( )
  // flps = flps_systemnew ( fill )
  // flps = flps_systemnew ( "empty" )
  // flps = flps_systemnew ( "current" )
  // flps = flps_systemnew ( "IEEEsingle" )
  // flps = flps_systemnew ( "IEEEdouble" )
  // flps = flps_systemnew ( "IEEEdoubleext" )
  // flps = flps_systemnew ( "format" , radix , p , ebits )
  //
  // Parameters
  //   fill : a 1x1 matrix of strings, the content of the new floating point system. Available values are fill="empty", "current", "IEEEdouble", "IEEEsingle", "IEEEdoubleext". Default is fill="empty". See below for details.
  //   radix : a 1x1 matrix of floating point integers, the radix
  //   p : a 1x1 matrix of floating point integers, the precision, i.e. the number of digits in the significand
  //   ebits : a 1x1 matrix of floating point integers, the number of digits in the exponent
  //   flps : a floating point system
  //   flps.radix: a 1x1 matrix of floating point integers, the basis for floating point numbers
  //   flps.p : a 1x1 matrix of floating point integers, the precision
  //   flps.emin : a 1x1 matrix of doubles, the minimum exponent before underflow
  //   flps.vmin : a 1x1 matrix of doubles, the minimum (normalized) value before underflow
  //   flps.emax : a 1x1 matrix of doubles, the maximum exponent before overflow
  //   flps.vmax : a 1x1 matrix of doubles, the maximum value before overflow
  //   flps.eps : a 1x1 matrix of doubles, the smallest value so that 1+eps <> 1 (the machine epsilon)
  //   flps.r : a 1x1 matrix of booleans, the rounding mode. The value r = 1 means round-to-nearest, r=2 means round-up, r=3 means round-down, r=4 means round-to-zero.
  //   flps.gu : a 1x1 matrix of booleans, %t if the machine supports graceful underflow. False if not. IEEE machines must support graceful underflow.
  //   flps.alpha : a 1x1 matrix of doubles, the smallest nonzero subnormal number
  //   flps.ebits : a 1x1 matrix of doubles, the number of bits in the exponent. This is called "w" in the IEEE standard.
  //
  // Description
  //   The available values of fill are the following.
  //   <itemizedlist>
  //   <listitem><para>If fill = "empty", an empty floating point system is returned. It is equivalent to "flps = flps_systemnew ( )". </para></listitem>
  //   <listitem><para>If fill = "current", then compute the current properties of the system and fill the fields with them. </para></listitem>
  //   <listitem><para>If fill = "IEEEsingle", then a IEEE single precision floating point system. This corresponds to radix=2, p=24, ebits=8.</para></listitem>
  //   <listitem><para>If fill = "IEEEdouble", then a IEEE double precision floating point system. This corresponds to radix=2, p=53, ebits=11.</para></listitem>
  //   <listitem><para>If fill = "IEEEdoubleext", then a IEEE double extended precision floating point system. This corresponds to radix=2, p=64, ebits=15 .</para></listitem>
  //   </itemizedlist>
  //
  //   The calling sequence flps = flps_systemnew ( ) creates an empty floating point
  //   system.
  //
  //   The calling sequence flps = flps_systemnew ( "empty" ) computes the properties
  //   of the current system and sets them into flps.
  //   This automatically computes the radix and rounding mode for Scilab doubles.
  //   Caution: the current detected rounding mode is only round-to-nearest.
  //   Other rounding modes are not detected by this function, i.e.
  //   if flps.r is not equal to 1, it is equal to %nan.
  //   Compute automatically the machine epsilon and the precision for Scilab doubles.
  //   Compute automatically the maximum value and exponent.
  //   Compute automatically the minimum value and exponent and the
  //   graceful underflow field.
  //
  //   We do not include the unit roundoff u into flps.
  //   If we did, and if the user changes the rounding mode,
  //   then the unit roundoff should be updated accordingly.
  //   Since there is currently no system to update u automatically,
  //   it is safer not to store u.
  //
  //   Caution! The Double Extended floating point system which is defined by flps_systemnew
  //   contains wrong fields.
  //   This is because we store the internal parameters with doubles.
  //   Hence, the vmin, vmax and alpha parameters cannot be represented and are wrong.
  //   We have vmin=0 (correct ~ 3.362E-4932), vmax=%inf (correct ~ 1.189E+4932)
  //   and alpha = 0.
  //
  //
  // Examples
  // // Create an empty floating point system
  // flps = flps_systemnew ()
  // flps.radix = 2
  // flps.p = 12
  // // Convert the floating point system into a string
  // string(flps)
  //
  // // Create an empty floating point system
  // flps = flps_systemnew ( "empty" )
  //
  // // Compute the current floating point properties
  // flps = flps_systemnew ( "current" )
  //
  // // IEEE single
  // flps = flps_systemnew ( "IEEEsingle" )
  // // ... is the same as ...
  // flps = flps_systemnew ( "format" , 2 , 24 , 8 )
  //
  // // IEEE double
  // flps = flps_systemnew ( "IEEEdouble" )
  // // ... is the same as ...
  // flps = flps_systemnew ( "format" , 2 , 53 , 1 )
  //
  // // IEEE double extended
  // flps = flps_systemnew ( "IEEEdoubleext" )
  // // ... is the same as ...
  // flps = flps_systemnew ( "format" , 2 , 64 , 15 )
  //
  // Authors
  // Copyright (C) 2008 - 2010 - Michael Baudin
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_systemnew" , rhs , 0:4 )
  apifun_checklhs ( "flps_systemnew" , lhs , 1 )
  //
  fill = argindefault ( rhs , varargin , 1 , "empty" )
  //
  apifun_checktype ( "flps_systemnew" , fill , "fill" , 1 , "string" )
  //
  apifun_checkscalar ( "flps_systemnew" , fill , "fill" , 1 )
  //
  apifun_checkoption ( "flps_systemnew" , fill , "fill" , 1 , ["empty" "current" "IEEEsingle" "IEEEdouble" "IEEEdoubleext" "format"] )
  //
  // Create an empty floating point system
  flps = flps_systemnew0args ( )
  if ( rhs == 0 ) then
    return
  end
  //
  if ( fill == "current" ) then
    apifun_checkrhs ( "flps_systemnew" , rhs , 1 )
    //
    flps = flps_systemnew_current ( flps )
  elseif ( fill == "IEEEsingle" ) then
    apifun_checkrhs ( "flps_systemnew" , rhs , 1 )
    //
    flps = flps_IEEEsingle ( )
  elseif ( fill == "IEEEdouble" ) then
    apifun_checkrhs ( "flps_systemnew" , rhs , 1 )
    //
    flps = flps_IEEEdouble ( )
  elseif ( fill == "IEEEdoubleext" ) then
    apifun_checkrhs ( "flps_systemnew" , rhs , 1 )
    //
    flps = flps_IEEEdoubleext ( )
  elseif ( fill == "format" ) then
    apifun_checkrhs ( "flps_systemnew" , rhs , 4 )
    //
    radix = varargin(2)
    p = varargin(3)
    ebits = varargin(4)
    //
    apifun_checktype ( "flps_systemnew" , radix , "radix" , 2 , "constant" )
    apifun_checktype ( "flps_systemnew" , p ,     "p" ,     3 , "constant" )
    apifun_checktype ( "flps_systemnew" , ebits , "ebits" , 4 , "constant" )
    //
    apifun_checkscalar ( "flps_systemnew" , radix , "radix" , 2 )
    apifun_checkscalar ( "flps_systemnew" , p ,     "p" ,     3 )
    apifun_checkscalar ( "flps_systemnew" , ebits , "ebits" , 4 )
    //
    apifun_checkgreq ( "flps_systemnew" , radix , "radix" , 2 , 1 )
    apifun_checkgreq ( "flps_systemnew" , p ,     "p" ,     3 , 1 )
    apifun_checkgreq ( "flps_systemnew" , ebits , "ebits" , 4 , 1 )
    //
    flps = flps_format2system ( radix , p , ebits )
  end
endfunction

function flps = flps_systemnew0args ( )
  flps = tlist(["TFLSYS"
    "radix"
    "p"
    "emin"
    "emax"
    "vmin"
    "vmax"
    "eps"
    "r"
    "gu"
    "alpha"
    "ebits"
    ]);
    flps.radix = []
    flps.p = []
    flps.emin = []
    flps.emax = []
    flps.vmin = []
    flps.vmax = []
    flps.eps = []
    flps.r = []
    flps.gu = []
    flps.alpha = []
    flps.ebits = []
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

function flps = flps_systemnew_current ( flps )
    [ radix , rounding ] = flps_radix ();
    [eps,p] = flps_eps (radix);
    [ emax , vmax ] = flps_emax ( radix , p );
    [ emin , vmin , gu , alpha ] = flps_emin ( radix , p );
    flps.radix = radix
    flps.p = p
    flps.emin = emin
    flps.emax = emax
    flps.eps = eps
    flps.vmin = vmin
    flps.vmax = vmax
    if ( rounding ) then
      flps.r = 1
    else
      flps.r = %nan
    end
    flps.gu = gu
    flps.alpha = alpha
    flps.ebits = int(log2(emax+1)) + 1
endfunction


function flps = flps_IEEEsingle ( )
  flps = flps_systemnew0args ( );
  flps.radix = 2
  flps.p = 24
  flps.emin = -126
  flps.emax = 127
  flps.vmin = 1.175494350822287508e-38
  flps.vmax = 3.402823466385288598D+38
  flps.eps = 1.19209289550781250e-007
  flps.r = 1
  flps.gu = %t
  flps.alpha = 1.401298464324817071e-45
  flps.ebits = 8
endfunction

function flps = flps_IEEEdouble ( )
  flps = flps_systemnew0args ( );
  flps.radix = 2
  flps.p = 53
  flps.emin = -1022
  flps.emax = 1023
  flps.vmin = 2.225073858507201383e-308
  flps.vmax = 1.79769313486231570D+308
  flps.eps = 2.22044604925031310E-016
  flps.r = 1
  flps.gu = %t
  flps.alpha = 4.940656458412465442e-324
  flps.ebits = 11
endfunction

function flps = flps_IEEEdoubleext ( )
  flps = flps_systemnew0args ( );
  flps.radix = 2
  flps.p = 64
  flps.emin = -16382
  flps.emax = 16383
  flps.vmin = 0
  flps.vmax = %inf
  flps.eps = 1.08420217248550440e-019
  flps.r = 1
  flps.gu = %t
  flps.alpha = 0
  flps.ebits = 15
endfunction

function flps = flps_format2system ( radix , p , ebits )
  flps = flps_systemnew0args ( );
  flps.radix = radix
  flps.p = p
  flps.emin = -radix^(ebits-1) + 2
  flps.emax = radix^(ebits-1)-1
  flps.vmin = radix^(flps.emin)
  flps.vmax = (radix - radix^(1-p)) * radix^(flps.emax)
  flps.eps = radix^(1-p)
  flps.r = 1
  flps.gu = %t
  flps.alpha = radix^(flps.emin-p+1)
  flps.ebits = ebits
endfunction

