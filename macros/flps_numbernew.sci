// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function flpn = flps_numbernew ( varargin )
  //   Returns a new floating point number.
  //
  // Calling Sequence
  //   flpn = flps_numbernew ( )
  //   flpn = flps_numbernew ( "integral" , flps , M , e )
  //   flpn = flps_numbernew ( "signm" , flps , s , m , e )
  //   flpn = flps_numbernew ( "double" , flps , x )
  //   flpn = flps_numbernew ( "hex" , flps , hexstr )
  //
  // Parameters
  //   flps : a floating point system
  //   rtype : a 1x1 matrix of strings, the type of representation, rtype="integral", rtype="signm", rtype="double"
  //   M : a 1x1 matrix of floating point integers, the integral significand
  //   e : a 1x1 matrix of floating point integers, the exponent
  //   s : a 1x1 matrix of floating point integers, the sign
  //   m : a 1x1 matrix of floating point integers, the normal significand
  //   x : a 1x1 matrix of doubles, the floating point number
 //    hexstr : a 1x1 matrix of strings, with length 16.
  //   flpn: a floating point number
  //   flpn.sign: 1x1 matrix of floating point integers, 0 if x is positive, 1 if x is negative
  //   flpn.M : a 1x1 matrix of floating point integers, the integral significant, so that x = M * b^(e-p+1)
  //   flpn.m : a 1x1 matrix of floating point integers, the normal significant, so that x = (-1)^s * m * b^e and 0 <= m < beta
  //   flpn.e : a 1x1 matrix of floating point integers, the exponent
  //   flpn.flps : the floating point system supporting the number
  //
  // Description
  //   The "flpn = flps_numbernew ( )" creates an empty floating point number.
  //
  //   If the "integral" representation is chosen,
  //   returns a new floating point number with given (M,e) representation.
  //   The sign s is computed by the rule s=0 is for x >= 0,
  //   s=1 is for x < 0.
  //   The normal significant is computed by the rule m = |M| * radix^(1-p).
  //   No negative zero can be produced with this "integral" constructor.
  //
  //   Returns a new floating point number with given (s,m,e) representation.
  //   The integral significant is computed by the rule M = (-1)^s * m * radix^(p - 1).
  //
  //   When rtype="double" and x=%nan, we set the sign bit to 1, the exponent bits to 1 and the
  //   significand bits to 1.
  //   This corresponds to a IEEE quiet nan.
  //   We do not have any mean in Scilab to distinguish
  //   a quiet nan and a signaling nan.
  //
  // Examples
  // // Empty constructor
  // flpn = flps_numbernew ( )
  //
  // // A toy system
  // radix = 2;
  // p = 3;
  // ebits = 3;
  // flps = flps_systemnew ( "format" , radix , p , ebits );
  // flpn = flps_numbernew ( "integral" , flps , 4 , 2 )
  // string(flpn)
  // flpn = flps_numbernew ( "signm" , flps , 0 , 1 , 2 )
  // string(flpn)
  //
  // // Create numbers from actual doubles
  // flps = flps_systemnew ( "IEEEsingle" );
  // flpn = flps_numbernew ( "double" , flps , 1/3 )
  // flpn = flps_numbernew ( "double" , flps , 3*2^-128 )
  // // Check rounding modes : default mode is round to nearest
  // flps = flps_systemnew ( "format" , 2 , 3 , 3 );
  // flpn = flps_numbernew ( "double" , flps , 0.54 )
  // flpn = flps_numbernew ( "double" , flps , 0.60 )
  // flpn = flps_numbernew ( "double" , flps , -0.54 )
  // flpn = flps_numbernew ( "double" , flps , -0.60 )
  // // A case of tie: check that round to even is applied
  // x= 4.5 * 2^(-1-3+1);
  // flpn = flps_numbernew ( "double" , flps , x )
  // // A case of tie: check that round to even is applied
  // x= 4.5 * 2^(0-3+1);
  // flpn = flps_numbernew ( "double" , flps , x )
  // // A case of tie: check that round to even is applied
  // x= -4.5 * 2^(0-3+1);
  // flpn = flps_numbernew ( "double" , flps , x )
  // // A case where the number is rounded up, which makes the exponent increase
  // // to keep the number normalized
  // flpn = flps_numbernew ( "double" , flps , 0.49 )
  // // Check rounding modes : set rounding mode to round up
  // flps = flps_systemnew ( "format" , 2 , 3 , 3 );
  // flps.r = 2;
  // flpn = flps_numbernew ( "double" , flps , 0.54 )
  // flpn = flps_numbernew ( "double" , flps , 0.60 )
  // flpn = flps_numbernew ( "double" , flps , -0.54 )
  // flpn = flps_numbernew ( "double" , flps , -0.60 )
  // // A case where the number is rounded up, which makes the exponent increase
  // // to keep the number normalized
  // flpn = flps_numbernew ( "double" , flps , 0.99 )
  // // Check rounding modes : set rounding mode to round down
  // flps = flps_systemnew ( "format" , 2 , 3 , 3 );
  // flps.r = 3;
  // flpn = flps_numbernew ( "double" , flps , 0.54 )
  // flpn = flps_numbernew ( "double" , flps , 0.60 )
  // flpn = flps_numbernew ( "double" , flps , -0.54 )
  // flpn = flps_numbernew ( "double" , flps , -0.60 )
  // // A case where the number is rounded up, which makes the exponent decrease
  // // to keep the number normalized
  // flpn = flps_numbernew ( "double" , flps , -0.49 )
  // // Check rounding modes : set rounding mode to round to zero
  // flps = flps_systemnew ( "format" , 2 , 3 , 3 );
  // flps.r = 4;
  // flpn = flps_numbernew ( "double" , flps , 0.54 )
  // flpn = flps_numbernew ( "double" , flps , 0.60 )
  // flpn = flps_numbernew ( "double" , flps , -0.54 )
  // flpn = flps_numbernew ( "double" , flps , -0.60 )
  //
  // // Create numbers from doubles
  // flps = flps_systemnew ( "IEEEdouble" );
  // flpn = flps_numbernew ( "double" , flps , +0 )
  // flpn = flps_numbernew ( "double" , flps , -0 )
  // flpn = flps_numbernew ( "double" , flps , +1 )
  // flpn = flps_numbernew ( "double" , flps , -1 )
  // flpn = flps_numbernew ( "double" , flps , +%inf )
  // flpn = flps_numbernew ( "double" , flps , -%inf )
  // flpn = flps_numbernew ( "double" , flps , %nan )
  // flpn = flps_numbernew ( "double" , flps , +2-%eps )
  // flpn = flps_numbernew ( "double" , flps , -2+%eps )
  // flpn = flps_numbernew ( "double" , flps , flps.vmin )
  // flpn = flps_numbernew ( "double" , flps , flps.vmax )
  // flpn = flps_numbernew ( "double" , flps , flps.alpha )
  // flpn = flps_numbernew ( "double" , flps , flps.vmin*(1-flps.eps) )
  // flpn = flps_numbernew ( "double" , flps , flps.vmin*(1-2*flps.eps) )
  //
  // // Create numbers in integral form
  // flps = flps_systemnew("IEEEdouble");
  // // Create +Infinity
  // flps_numbernew ( "integral" , flps , +0 , flps.emax + 1 )
  // // Create -Infinity
  // flps_numbernew ( "integral" , flps , -0 , flps.emax + 1 )
  // // Create Nan
  // flps_numbernew ( "integral" , flps , +1 , flps.emax + 1 )
  // // Create +0
  // flps_numbernew ( "integral" , flps , +0 , flps.emin )
  // // Create -0
  // flps_numbernew ( "integral" , flps , -0 , flps.emin )
  // // Create Largest positive subnormal
  // flps_numbernew ( "integral" , flps , flps.radix^(flps.p-1)-1 , flps.emin )
  // // Create Largest negative subnormal
  // flps_numbernew ( "integral" , flps , -(flps.radix^(flps.p-1)-1) , flps.emin )
  // // Create Smallest positive subnormal
  // flps_numbernew ( "integral" , flps , +1 , flps.emin )
  // // Create Smallest negative subnormal
  // flps_numbernew ( "integral" , flps , -1 , flps.emin )
  //
  // // Create a number (a double) from an hex
  // flps = flps_systemnew("IEEEdouble");
  // flpn = flps_numbernew ( "hex" , flps , "400921FB54442D18" )
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010
  //   http://en.wikipedia.org/wiki/Double_precision


  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_numbernew" , rhs , [0 3 4 5] )
  apifun_checklhs ( "flps_numbernew" , lhs , 1 )
  //
  if ( rhs == 0 ) then
    flpn = flps_numbernew_0arg ( )
    return
  end
  rtype = varargin(1)
  apifun_checkoption ( "flps_numbernew" , rtype , "rtype" , 1 , ["integral" "signm" "double" "hex"] )
  //
  if ( rtype == "integral" ) then
    apifun_checkrhs ( "flps_numbernew" , rhs , 4 )
    //
    flps = varargin(2)
    M = varargin(3)
    e = varargin(4)
    //
    apifun_checktype ( "flps_numbernew" , flps , "flps" , 2 , "TFLSYS" )
    apifun_checktype ( "flps_numbernew" , M , "M" , 3 , "constant" )
    apifun_checktype ( "flps_numbernew" , e , "e" , 4 , "constant" )
    //
    // Check values
    radix = flps.radix
    p = flps.p
    bias = flps.emax
    emin = flps.emin
    emax = flps.emax
    //
    Mmax = radix^p-1
    Mmin = radix^(p-1)
    apifun_checkrange ( "flps_numbernew" , M , "M" , 3 , -Mmax , Mmax )
    apifun_checkrange ( "flps_numbernew" , e , "e" , 4 , emin , emax + 1 )
    //
    if ( e <= emax ) then
      if ( e>emin & abs(M)<Mmin ) then
        // Number should be normal
        localstr = gettext("%s: Exponent e=%s is for a normal number, but abs(M)=%s in not in the range [%s,%s].")
        msg = msprintf(localstr,"flps_numbernew",string(e),string(abs(M)),string(Mmin),string(Mmax))
        error(msg)
      end
    end
    //
    flpn = flps_numbernew_integral ( flps , M , e )
  elseif ( rtype == "signm" ) then
    apifun_checkrhs ( "flps_numbernew" , rhs , 5 )
    //
    flps = varargin(2)
    s = varargin(3)
    m = varargin(4)
    e = varargin(5)
    //
    apifun_checktype ( "flps_numbernew" , flps , "flps" , 2 , "TFLSYS" )
    apifun_checktype ( "flps_numbernew" , s , "s" , 3 , "constant" )
    apifun_checktype ( "flps_numbernew" , m , "m" , 4 , "constant" )
    apifun_checktype ( "flps_numbernew" , e , "e" , 5 , "constant" )
    //
    //
    // Check values
    radix = flps.radix
    p = flps.p
    bias = flps.emax
    emin = flps.emin
    emax = flps.emax
    //
    apifun_checkrange ( "flps_numbernew" , m , "m" , 4 , 0 , radix )
    apifun_checkrange ( "flps_numbernew" , e , "e" , 5 , emin , emax + 1 )
    apifun_checkoption ( "flps_numbernew" , s , "s" , 3 , [0 1] )
    //
    if ( m == radix ) then
      localstr = gettext("%s: Normal significant m=%s cannot be equal to the radix=%s.")
      errmsg = msprintf(localstr,"flps_numbernew",string(m),string(radix))
      error(errmsg)
    end
    if ( e <= emax ) then
      // Number should be normal
      if ( e>emin & m<1 ) then
        localstr = gettext("%s: Exponent e=%s is for a normal number, but m=%s in not in the range [%s,%s].")
        msg = msprintf(localstr,"flps_numbernew",string(e),string(m),string(1),string(radix))
        error(msg)
      end
    end
    //
    flpn = flps_numbernew_signm ( flps , s , m , e )
  elseif ( rtype == "double" ) then
    apifun_checkrhs ( "flps_numbernew" , rhs , 3 )
    //
    flps = varargin(2)
    x = varargin(3)
    //
    apifun_checktype ( "flps_numbernew" , flps , "flps" , 2 , "TFLSYS" )
    apifun_checktype ( "flps_numbernew" , x , "x" , 3 , "constant" )
    //
    apifun_checkscalar ( "flps_numbernew" , x , "x" , 3 )
    //
    flpn = flps_numbernew_double ( flps , x )
  elseif ( rtype == "hex" ) then
    apifun_checkrhs ( "flps_numbernew" , rhs , 3 )
    //
    flps = varargin(2)
    hex = varargin(3)
    //
    apifun_checktype ( "flps_numbernew" , flps , "flps" , 2 , "TFLSYS" )
    apifun_checktype ( "flps_numbernew" , hex , "hex" , 3 , "string" )
    //
    apifun_checkscalar ( "flps_numbernew" , hex , "hex" , 3 )
    //
    flpn = flps_numbernew_hex ( flps , hex )
  else
    errmsg = msprintf(gettext("%s: Unexpected representation type: %s provided while ""integral"" or ""signm"" are expected."), "flps_numbernew", string(rtype));
    error(errmsg)
  end
endfunction

function flpn = flps_numbernew_0arg ( )
  //
  // Creates an empty floating point number
  flpn = tlist(["TFLNUMB"
    "s"
    "M"
    "m"
    "e"
    "flps"
    ])
  flpn.s = []
  flpn.M = []
  flpn.m = []
  flpn.e = []
  flpn.flps = flps_systemnew ( )
endfunction

function flpn = flps_numbernew_integral ( flps , M , e )
  //
  // Creates a floating point number from its (M,e) representation
  flpn = flps_numbernew_0arg ( )
  flpn.flps = flps
  flpn.e = e
  flpn.M = M
  [s,m] = flps_Me2sm ( flps.radix , flps.p , M , e )
  flpn.s = s
  flpn.m = m
endfunction

function flpn = flps_numbernew_signm ( flps , s , m , e )
  //
  // Creates a floating point number from its (s,m,e) representation
  flpn = flps_numbernew_0arg ( )
  flpn.flps = flps
  flpn.s = s
  flpn.m = m
  flpn.e = e
  M = flps_sme2M ( flps.radix , flps.p , s , m , e )
  flpn.M = M
endfunction


function flpn = flps_numbernew_double ( flps , x )
  //
  // Creates a floating point number from its double representation
  //
  // 0. Create the floating point number
  flpn = flps_numbernew_0arg ( )
  flpn.flps = flps
  //
  // 0.1 Store necessary fields into basic variables (faster and simpler)
  radix = flps.radix
  r = flps.r
  p = flps.p
  emax = flps.emax
  emin = flps.emin
  //
  // 1. Compute e
  if ( isnan(x) ) then
    e = emax + 1
    M = number_frombary(ones(p,1))
  elseif ( x == %inf ) then
    e = emax + 1
    M = +0
  elseif ( x == -%inf ) then
    e = emax + 1
    M = -0
  elseif ( x == 0 ) then
    e = emin
    // Keep the sign of x
    M = x
  else
    [ f , e ] = flps_frexp ( x , radix , "v2" )
    e = min(max(e,emin),emax)
    //
    // 2. Compute M, update e if required
    [M,e] = flps_numbformcompM ( r , x , radix, p , e )
  end
  //
  // 3. Compute s, m
  [s,m] = flps_Me2sm ( radix , p , M , e )
  //
  // 4. Store the fields
  flpn.e = e
  flpn.M = M
  flpn.s = s
  flpn.m = m
endfunction

function [M,e] = flps_numbformcompM ( r , x , radix, p , e )
  // Compute M depending on x.
  // Update e, if required.
  select r
  case 1
    // Rounding= To Nearest
    // Compute the infinitely precise significant
    // Avoid dividing by zero in case of denormals
    if ( e == emin - 1 ) then
      Minf = (x * radix^(p-1)) / radix^e
    else
      Minf = x / radix^(e-p+1)
    end
    // Compute the two integral significant which are closest to Minf: M1 <= Minf <= M2
    M1 = floor(Minf)
    M2 = ceil(Minf)
    if ( M2-Minf <> Minf - M1 ) then
      // If there is no tie, round to nearest
      M = round(Minf)
      if ( abs(M) < radix^p ) then
        M = M
      else
        // If rounding to nearest is up and the number becomes denormal
        // increase the exponent.
        M = M/2
        e = e + 1
      end
    else
      // Select the even one
      if ( modulo(M1,2)==0 ) then
        M = M1
      else
        M = M2
      end
    end
  case 2
    // Rounding= Up
    M = ceil(x / radix^(e-p+1))
    // If rounding to nearest is up and the number becomes denormal
    // increase the exponent.
    if ( abs(M) < radix^p ) then
      M = M
    else
      M = M/2
      e = e + 1
    end
  case 3
    // Rounding= Down
    M = floor(x / radix^(e-p+1))
    if ( abs(M) < radix^p ) then
      M = M
    else
      M = M/2
      e = e + 1
    end
  case 4
    // Rounding= To Zero
    M = int(x / radix^(e-p+1))
  else
    errmsg = msprintf(gettext("%s: Unexpected rounding mode: %d."), "flps_numbernew_double", r);
    error(errmsg)
  end
  // Fix the bit sign of M if x=0
  if ( x == 0 ) then
    sb = flps_signbit(x)
    if ( sb ) then
      M = -0
    end
  end
endfunction

function flpn = flps_numbernew_hex ( flps , hexstr )
  //
  // Creates a floating point number from its hex representation
  //
  // Extract the parts of the number
  ebits = flps.ebits;
  p = flps.p;
  //
  // Check that the length of the number matches the
  // current floating point system.
  lenstr = length(hexstr)
  if ( ebits+p <> 4*lenstr ) then
    localstr = gettext("%s: The length of the hex string is %d which does not match the current floating point system with ebits=%d and p=%d.")
    errmsg = msprintf(localstr, "flps_numbernew_hex", lenstr,ebits,p);
    error(errmsg)
  end
  //
  //
  binstr = number_hex2bin ( hexstr )
  //
  // Extracts the parts of the binary string
  s_str = part(binstr,1)
  e_str = part(binstr,2:ebits+1)
  M_str = part(binstr,ebits+2:ebits+p)
  //
  // Convert the sign into 0/1 digits, then integer
  s_d = evstr(strsplit(s_str))
  s = number_frombary ( s_d )
  //
  // Convert the exponent
  e_d = evstr(strsplit(e_str))
  e = number_frombary ( e_d )
  //
  // Convert the string into 0/1 digits
  M_d = evstr(strsplit(M_str))
  //
  // Apply the offset on the exponent
  e = e - flps.emax
  //
  // Update e and M depending on the exponent
  if ( e == flps.emin - 1 ) then
    // Number is denormal
    M_d = [0;M_d]
    e = flps.emin
  elseif ( e > flps.emax ) then
    if ( and(M_d==0) ) then
      // Number is infinite
      M_d = [0;M_d]
    else
      // Number is nan
      // Adding 1 as leading implicit bit is a choice:
      // the standard does not specify this.
      M_d = [1;M_d]
    end
  else
    // Number is normal
    // Add the implicit bit
    M_d = [1;M_d]
  end
  //
  // Convert M into integer
  M = number_frombary ( M_d )
  //
  // Apply the sign on M
  if ( s == 1 ) then
    M = -M
  end
  //
  // Create the number from the (M,e) representation
  flpn = flps_numbernew_integral ( flps , M , e )
endfunction

