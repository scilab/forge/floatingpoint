// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function ystr = flps_minimumdecimalstr ( xstr )
  // Returns the minimum string for equality.
  //
  // Calling Sequence
  //   ystr = flps_minimumdecimalstr ( xstr )
  //
  // Parameters
  //   xstr : a m-by-n matrix of strings, the input floating point numbers
  //   ystr : a m-by-n matrix of strings, the output floating point numbers such that evstr(xstr)==evstr(ystr)
  //
  // Description
  //   The decimal to binary conversion is not exact.
  //   Sometimes, a decimal string representing a double precision floating point number
  //   contains more digits than necessary.
  //   The algorithm provided by the current function removed the unnecessary
  //   decimal digits, so that the output decimal string contains the
  //   least possible number of digits which allows to get the exact double.
  //
  //   Caution: this function does not work with strings containing exponents.
  //
  // Examples
  // format("v",25)
  // //
  // x = 1.1 // Displays 1.1000000000000000888178
  // xstr = string(x) // Displays "1.1000000000000000888178"
  // ystr = flps_minimumdecimalstr ( xstr ) // Displays "1.1"
  // y = evstr(ystr) // Displays 1.1000000000000000888178
  // //
  // flps_minimumdecimalstr ( "1.1000000000000000888178e-010" ) // "1.10000000000000008e-10"
  // flps_minimumdecimalstr ( "1.1000000000000000888178e+010" ) // "1.1e-10"
  // flps_minimumdecimalstr ( "0000000.00000001" ) // ".00000001"
  // flps_minimumdecimalstr ( "2.00047180001120725D-320" ) // "2.0004D-320"
  // flps_minimumdecimalstr ( "000000.0000000" ) // "0."
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_minimumdecimalstr" , rhs , 1 )
  apifun_checklhs ( "flps_minimumdecimalstr" , lhs , 1 )
  //
  apifun_checktype ( "flps_minimumdecimalstr" , xstr , "xstr" , 1 , "string" )
  //
  if ( ~and(isnum(xstr)) ) then
    k = find(~isnum(xstr),1)
    localstr = gettext("%s: Expected only numbers in input argument #%d, but variable %s contains also a non-number at entry #%d: %s.")
    errmsg = msprintf(localstr,"flps_minimumdecimalstr",1,"xstr",k,xstr(k))
    error(errmsg)
  end
  //
  ystr = xstr
  //
  // Try to reduce the digits as much as possible
  nrows = size(xstr,"r")
  ncols = size(xstr,"c")
  for i = 1 : nrows
  for j = 1 : ncols
    digits = flps_mindecstr_simplify ( ystr(i,j) )
    if ( digits <> ystr(i,j) ) then
      ystr(i,j) = digits
    end
  end
  end
endfunction


function digits = flps_mindecstr_simplify ( digits )
    spltdig = strsplit(digits)'
    t = evstr(digits)
    nd = size(spltdig,"*")
    //
    // Try to remove all digits #k from k=nd, to k=1.
    // If we were able to remove a digit, continue on the updated string.
    // If not stop.
    // If there is no digit to be removed anymore because the end of the
    // string is reached, finish.
    // If not, go to the next char.
    k = nd
    while ( %t )
      if ( nd == 1 ) then
        break
      end
      //
      // Remove char #k
      newxsplit = spltdig
      remchar = newxsplit(k)
      newxsplit(k) = []
      newx = strcat(newxsplit,"")
      //
      // If the value does not change, remove char #k.
      if ( isnum(newx) ) then
      if ( evstr(newx) == t & and(remchar<>["+" "-" "D" "E" "e" "d" "."]) ) then
        spltdig = newxsplit
        digits = newx
        nd = nd - 1
        k = k - 1
        continue
      else
        break
      end
      end
      if ( k == 1 ) then
        break
      end
      k = k - 1
    end
    //
    // If there is an exponent, try to remove digits from there, toward the begining
    k = find(spltdig=="e"|spltdig=="d"|spltdig=="E"|spltdig=="D")
    if ( k <> [] ) then
    k = k - 1
    while ( %t )
      if ( nd == 1 ) then
        break
      end
      //
      // Remove char #k
      newxsplit = spltdig
      remchar = newxsplit(k)
      newxsplit(k) = []
      newx = strcat(newxsplit,"")
      //
      // If the value does not change, remove char #k.
      if ( isnum(newx) ) then
      if ( evstr(newx) == t & and(remchar<>["+" "-" "D" "E" "e" "d" "."]) ) then
        spltdig = newxsplit
        digits = newx
        nd = nd - 1
        k = k - 1
        continue
      else
        break
      end
      end
      if ( k == 1 ) then
        break
      end
      k = k - 1
    end
    end
    //
    // Try to remove all digits #k from k=1, to k=nd.
    // If we were able to remove a zero digit, continue on the updated string.
    // If there is no digit to be removed anymore because the end of the
    // string is reached, finish.
    // If not, go to the next char.
    k = 1
    while ( %t )
        if ( nd == 1 ) then
            break
        end
        //
        // Remove char #k
        newxsplit = spltdig
        remchar = newxsplit(k)
        newxsplit(k) = []
        newx = strcat(newxsplit,"")
        //
        // If the value does not change, remove char #k.
        if isnum(newx) then
            try // exec(".e10") throws an error in scilab 6
                if ( evstr(newx) == t & remchar == "0" ) then
                    spltdig = newxsplit
                    digits = newx
                    nd = nd - 1
                    k = k - 1; // compensate for the fact that continue is caught by try
                    continue
                end
            catch
            end
        end
        if k == nd then
            break
        end
        k = k + 1
    end
endfunction

