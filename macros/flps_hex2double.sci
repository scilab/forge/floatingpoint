// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = flps_hex2double ( hexstr )
 // Converts a hexadecimal string into its double.
 //
 // Calling Sequence
 //   x = flps_hex2double ( hexstr )
 //
 // Parameters
 //   hexstr : a 1x1 matrix of strings, with length 16.
 //   x : a 1x1 matrix of doubles
 //
 // Description
 //   Returns the double corresponding to the hexadecimal string.
 //   In the binary string, the characters are structured as following:
 //   <itemizedlist>
 //   <listitem><para>at index 1, the sign bit,</para></listitem>
 //   <listitem><para>at index 2 to 12, the biased exponent (with offset = 1023),</para></listitem>
 //   <listitem><para>at index 13 to 64, the significant.</para></listitem>
 //   </itemizedlist>
 //
 // Examples
 // expected = %pi;
 // x = flps_hex2double ("400921FB54442D18")
 //
 //
 // Authors
 // Copyright (C) 2010 - DIGITEO - Michael Baudin
 //

 [lhs, rhs] = argn()
 apifun_checkrhs ( "flps_hex2double" , rhs , 1 )
 apifun_checklhs ( "flps_hex2double" , lhs , 1 )
 //
 apifun_checktype ( "flps_hex2double" , hexstr , "hexstr" , 1 , "string" )
 //
 flps = flps_systemnew ( "IEEEdouble" )
 flpn = flps_numbernew ( "hex" , flps , hexstr )
 //
 x = flps_numbereval ( flpn )
endfunction


