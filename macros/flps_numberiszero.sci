// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function is = flps_numberiszero ( flpn )
  // Returns true if the number is zero.
  //
  // Calling Sequence
  //   is = flps_numberiszero ( flpn )
  //
  // Parameters
  //   flpn : a floating point number
  //   is : a 1x1 matrix of boolean
  //
  // Description
  //   Returns true if the number is a signed zero.
  //   A zero has a minimum exponent to its maximum value and a zero integral
  //   significant.
  //   The implicit leading significand bit is then zero.
  //
  // Examples
  // flps = flps_systemnew ( "IEEEdouble" );
  // // Get a zero
  // flpn = flps_numbernew ( "double" , flps , +0 );
  // is = flps_numberiszero ( flpn ) // %t
  // flpn = flps_numbernew ( "double" , flps , -0 );
  // is = flps_numberiszero ( flpn ) // %t
  //
  // // Get others
  // flpn = flps_numbernew ( "double" , flps , -%inf );
  // is = flps_numberiszero ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , %inf );
  // is = flps_numberiszero ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , %nan );
  // is = flps_numberiszero ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , 1 );
  // is = flps_numberiszero ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , -1 );
  // is = flps_numberiszero ( flpn ) // %f
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  is = ( flpn.e == flpn.flps.emin & abs(flpn.M) == 0 )
endfunction

