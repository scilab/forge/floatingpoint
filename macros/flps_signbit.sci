// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function b = flps_signbit ( x )
  //   Returns the sign bit of x
  //
  // Calling Sequence
  // b = flps_signbit ( x )
  //
  // Parameters
  //   x : a matrix of doubles
  //   b : a matrix of booleans. b(i,j)=%f if the sign bit of x(i,j) is 0 (i.e. x is positive). b(i,j)=%t if the sign bit of x(i,j) is 1 (i.e. x is negative).
  //
  // Description
  //   This functions allows to inquire about the sign bit of x.
  //   The sign bit s is so that the sign of x is (-1)^s.
  //
  // This allows to overcome the sign function, which returns the same
  // value for -0 and -0.
  //
  // This function works well for +/- inf.
  // For +/-%nan, this function returns %f.
  // The IEEE standard does not specify the sign bit of a nan.
  // See in the section 6.3 "The sign bit" of the IEEE 754-2008 Standard.
  //
  // Examples
  //   b = flps_signbit ( 1 ) // %f
  //   b = flps_signbit ( -1 ) // %t
  //   b = flps_signbit ( 0 ) // %f
  //   b = flps_signbit ( -0 ) // %t
  //
  //
  // x =        [+0 -0 %inf -%inf %nan -%nan];
  // expected = [%f %t %f   %t    %f   %f]
  // b = flps_signbit ( x )
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010
  //   "IEEE Standard for Floating-Point Arithmetic", IEEE Computer Society, IEEE Std 754-2008

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_signbit" , rhs , 1 )
  apifun_checklhs ( "flps_signbit" , lhs , 1 )
  //
  apifun_checktype ( "flps_signbit" , x , "x" , 1 , "constant" )
  //
  backieee = ieee()
  ieee(2)
  //
  b = (ones(x)<>ones(x))
  b(1 ./ x < 0) = %t
  b(x == %inf) = %f
  b(x == -%inf) = %t
  //
  ieee(backieee)
endfunction

