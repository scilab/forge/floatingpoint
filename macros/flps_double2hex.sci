// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [hexstr,binstr] = flps_double2hex ( x )
  // Converts a double into a hexadecimal string.
  //
  // Calling Sequence
  //   [hexstr,binstr] = flps_double2hex ( hexstr )
  //
  // Parameters
  //   x : a 1x1 matrix of doubles
  //   hexstr : a 1x1 matrix of strings, with length 16.
  //   binstr : a 1x1 matrix of strings, with length 64
  //
  // Description
  //   Returns the hexadecimal and binary strings corresponding to the given double.
  //   In the binary string, the characters are structured as following:
  //   <itemizedlist>
  //   <listitem><para>at index 1, the sign bit,</para></listitem>
  //   <listitem><para>at index 2 to 12, the biased exponent (with offset = 1023),</para></listitem>
  //   <listitem><para>at index 13 to 64, the significant.</para></listitem>
  //   </itemizedlist>
  //
  // Examples
  // expected = "400921FB54442D18";
  // hexstr = flps_double2hex (%pi)
  //
  // // Get and extract the binary string
  // [hexstr,binstr] = flps_double2hex (%pi)
  // sign_str = part(binstr,1) // sign_str="0"
  // expo_str = part(binstr,2:12) // expo_str="10000000000"
  // M_str = part(binstr,13:64) // M_str="1001001000011111101101010100010001000010110100011000"
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_double2hex" , rhs , 1 )
  apifun_checklhs ( "flps_double2hex" , lhs , 1:2 )
  //
  apifun_checktype ( "flps_double2hex" , x , "x" , 1 , "constant" )
  //
  flps = flps_systemnew ( "IEEEdouble" )
  flpn = flps_numbernew ( "double" , flps , x )
  //
  [hexstr,binstr] = flps_number2hex ( flpn )
endfunction

