// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ radix , rounding ] = flps_radix ()
  //   Compute the radix used for Scilab doubles.
  //
  // Calling Sequence
  //   radix = flps_radix ()
  //   [ radix , rounding ] = flps_radix ()
  //
  // Parameters
  //   radix : a 1x1 matrix of floating point integers, the radix of the floating point system
  //   rounding : a 1x1 matrix of booleans, true if round-to-nearest is enabled, false if not.
  //
  // Description
  //   This algorithm is a Scilab port of relevant parts
  //   of Lapack's DLAMCH. Initially, a simpler algorithm was used,
  //   but this algorithm did not allow to compute the rounding mode.
  //
  //   TODO : detect other types of rounding modes.
  //
  // Examples
  // [ radix , rounding ] = flps_radix ()
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Algorithms to Reveal Properties of Floating-Point Arithmetic", Michael A. Malcolm, Stanford University, Communications of the ACM, Volume 15,  Issue 11 (November 1972), Pages: 949 - 951
  //   "More on Algorithms that Reveal Properties of Floating Point Arithmetic Units", W. Morven Gentleman, University of Waterloo, Scott B. Marovich, Purdue University, Communications of the ACM, Volume 17,  Issue 5 (May 1974), Pages: 276 - 277
  //   "ALGORITHM 665 - MACHAR: A Subroutine to Dynamically Determine Machine Parameters", W. J. Cody, Argonne National Laboratory, ACM Transactions on Mathematical Software, Vol. 14, No. 4, December 1988, Pages 303-311.
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_radix" , rhs , 0 )
  apifun_checklhs ( "flps_radix" , lhs , 1:2 )
  //
  // Compute  a = 2.0**m  with the  smallest positive integer m such that
  //   fl( a + 1.0 ) = a.
  //
  a = 1.0
  while ( (a+1)-a == 1 )
    a = 2 * a
  end
  if ( %f ) then
    // This is a simpler algorithm to compute the radix,
    // which should work on most IEEE machines.
    radix = 1.0
    while ( (a+radix)-a <> radix )
      radix = radix + 1
    end
  end
  //
  // Now compute  radix = 2.0**m  with the smallest positive integer m such that
  //   fl( a + radix ) > a.
  //
  radix = 1.0
  c = a + radix
  while ( c == a )
    radix = 2 * radix
    c = a + radix
  end
  //
  // At this point we have a < c = a + radix
  // and a and c are two neighbouring floating point numbers.
  //
  //
  // Now compute the base.  a and c  are neighbouring floating point
  // numbers  in the  interval  ( beta**t, beta**( t + 1 ) )  and so
  // their difference is beta. Adding 0.25 to c is to ensure that it
  // is truncated to beta and not ( beta - 1 ).
  //
  radix = round ( (c - a) + 1 / 4 )
  //
  // Now determine whether rounding or chopping occurs,  by adding a
  // bit  less  than  beta/2  and a  bit  more  than  beta/2  to  a.
  //
  c1 = (radix / 2 - radix / 100) + a
  c2 = (radix / 2 + radix / 100) + a
  //
  // Try and decide whether rounding is done in the  IEEE  'round to
  // nearest' style. radix/2 is half a unit in the last place of the two
  // numbers A and c. Furthermore, A is even, i.e. has last  bit
  // zero, and c is odd. Thus adding radix/2 to A should not  change
  // A, but adding radix/2 to c should change c.
  //
  t1 = radix / 2 + a
  t2 = radix / 2 + c
  rounding = ( t1==a ) & ( t2>c ) & ( c1==a & c2 <> a )
endfunction

