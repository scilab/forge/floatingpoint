// Copyright (C) 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function listflpn = flps_systemall ( varargin )
  //   Returns the list of floating point numbers of the given floating point system.
  //
  // Calling Sequence
  //  listflpn = flps_systemall ( flps )
  //  listflpn = flps_systemall ( flps , denormals )
  //  listflpn = flps_systemall ( flps , denormals , onlypos )
  //
  // Parameters
  //   flps : a floating point system
  //   denormals : a 1x1 matrix of booleans, set to true to include denormalized numbers. Set to false to ignore denormalized numbers. Default is %t.
  //   onlypos : a 1x1 matrix of booleans, set to true to include only positive numbers. Set to false to include both positive and negative numbers. Default is %f.
  //   listflpn: a list, the list of normalized floating point number
  //
  // Description
  //   This function allows to get a list of floating point numbers
  //   of a toy floating point system.
  //
  // Any optional input argument equal to the empty matrix is replaced by its default value.
  //
  // Examples
  //   radix = 2;
  //   p = 3;
  //   e = 3;
  //   flps = flps_systemnew ( "format" , radix , p , e );
  //   // There are 56 f.p. numbers
  //   listflpn = flps_systemall ( flps );
  //   x = flps_numbereval ( listflpn )'
  //   // Ignore denormals
  //   listflpn = flps_systemall ( flps , %f );
  //   x = flps_numbereval ( listflpn )'
  //   // Get only positive numbers, ignore denormals
  //   listflpn = flps_systemall ( flps , %f , %t );
  //   x = flps_numbereval ( listflpn )'
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_systemall" , rhs , 1:3 )
  apifun_checklhs ( "flps_systemall" , lhs , 1 )
  //
  flps = varargin(1)
  denormals = argindefault ( rhs , varargin , 2 , %t )
  onlypos = argindefault ( rhs , varargin , 3 , %f )
  //
  apifun_checktype ( "flps_systemall" , flps ,      "flps" ,      1 , "TFLSYS" )
  apifun_checktype ( "flps_systemall" , denormals , "denormals" , 2 , "boolean" )
  apifun_checktype ( "flps_systemall" , onlypos ,   "onlypos" ,   3 , "boolean" )
  //
  apifun_checkscalar ( "flps_systemall" , denormals , "denormals" , 2 )
  apifun_checkscalar ( "flps_systemall" , onlypos , "onlypos" , 3 )
  //
  Mmin = flps.radix^(flps.p - 1)
  Mmax = flps.radix^flps.p - 1
  listflpn = list()
  if ( ~onlypos ) then
    // Negative normalized numbers
    for e = flps.emax : -1 : flps.emin
      for M = -Mmax : -Mmin
        listflpn($+1) = flps_numbernew ( "integral" , flps , M , e )
      end
    end
    // Negative denormalized numbers
    if ( denormals ) then
      e = flps.emin
      for M = -Mmin + 1 : -1
        listflpn($+1) = flps_numbernew ( "integral" , flps , M , e )
      end
    end
    // Add -zero
    listflpn($+1) = flps_numbernew ( "integral" , flps , -0 , flps.emin )
  end
  // Add +zero
  listflpn($+1) = flps_numbernew ( "integral" , flps , +0 , flps.emin )
  // Positive denormalized numbers
  if ( denormals ) then
    e = flps.emin
    for M = 1 : Mmin - 1
      listflpn($+1) = flps_numbernew ( "integral" , flps , M , e )
    end
  end
  // Positive normalized numbers
  for e = flps.emin : flps.emax
    for M = Mmin : Mmax
      listflpn($+1) = flps_numbernew ( "integral" , flps , M , e )
    end
  end
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

