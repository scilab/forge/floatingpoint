// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [hexstr,binstr] = flps_number2hex ( flpn )
  // Converts a floating point number into a hexadecimal string.
  //
  // Calling Sequence
  //   [hexstr,binstr] = flps_number2hex ( flpn )
  //
  // Parameters
  //   flpn : a floating point number
  //   hexstr : a 1x1 matrix of strings, with length 16.
  //   binstr : a 1x1 matrix of strings, with length 64
  //
  // Description
  //   Returns the hexadecimal and binary strings corresponding to the given floating point number.
  //   In the binary string, the characters are structured as following:
  //   <itemizedlist>
  //   <listitem><para>at index 1, the sign bit,</para></listitem>
  //   <listitem><para>at index 2 to flps.ebits+1, the biased exponent (with offset = flps.emax),</para></listitem>
  //   <listitem><para>at index flps.ebits+2 to flps.ebits+flps.p, the integral significant.</para></listitem>
  //   </itemizedlist>
  //
  // Examples
  // expected = "400921FB54442D18";
  // flps = flps_systemnew ( "IEEEdouble" );
  // flpn = flps_numbernew ( "double" , flps , %pi );
  // hexstr = flps_number2hex (flpn)
  //
  // // Get and extract the binary string
  // flps = flps_systemnew ( "IEEEdouble" );
  // ebits = flps.ebits;
  // p = flps.p;
  // flpn = flps_numbernew ( "double" , flps , %pi )
  // [hexstr,binstr] = flps_number2hex (flpn)
  // sign_str = part(binstr,1) // sign_str="0"
  // expo_str = part(binstr,2:ebits+1) // expo_str="10000000000"
  // M_str = part(binstr,ebits+2:ebits+p) // M_str="1001001000011111101101010100010001000010110100011000"
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_number2hex" , rhs , 1 )
  apifun_checklhs ( "flps_number2hex" , lhs , 1:2 )
  //
  apifun_checktype ( "flps_number2hex" , flpn , "flpn" , 1 , "TFLNUMB" )
  //
  flps = flpn.flps
  radix = flps.radix
  p = flps.p
  m = flpn.m
  M = flpn.M
  e = flpn.e
  emax = flps.emax
  //
  // Temporarily change the format, to make clean the
  // conversion to integer performed by the "string" function.
  backformat = format()
  format("v",10)
  //
  // 1. Create the string for the sign
  sign_str = string(flpn.s)
  //
  // 2. Create the string for the exponent
  if ( flpn.e == flps.emin & flpn.m < 1 ) then
    // Number is subnormal
    ebiased = 0
  else
    ebiased = e + emax
  end
  expo_str = number_tobary ( ebiased , 2 , "littleendian" , flps.ebits )
  expo_str = strcat(string(expo_str),"")
  //
  // 3. Create the string for the significant
  M_str = flps_tobary ( m , radix , p )
  // Remove the leading implicit bit
  M_str(1)=[]
  M_str = strcat(string(M_str),"")
  // length(sign_str) == 1
  // length(expo_str) == 11
  // length(M_str) == 52
  //
  // 4. Combine all strings and create the hex
  binstr = sign_str + expo_str + M_str
  hexstr = number_bin2hex(binstr)
  //
  // Put the format back in place
  if ( backformat(1) == 0 ) then
    format("e",backformat(2))
  else
    format("v",backformat(2))
  end
endfunction

