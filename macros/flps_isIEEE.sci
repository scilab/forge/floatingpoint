// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function isieee = flps_isIEEE ( )
  //   Returns true if the current system satisfies basic IEEE requirements.
  //
  // Calling Sequence
  //   isieee = flps_isIEEE ( )
  //
  // Parameters
  //   isieee: a 1x1 matrix of booleans, %t if the current system satisfies basic IEEE requirements
  //
  // Description
  //   This function calls flps_systemnew with fill="curren" and compare
  //   this with the result of flps_systemnew ( "IEEEdouble" ).
  //   If the two floating point systems are the same, we return %t and
  //   return %f if not.
  //
  //   This function may return false if graceful underflow is not supported,
  //   that is, the flps.gu field is false.
  //
  //   If may also return false if the rounding mode is not round-to-nearest,
  //   that is, the flps.r field is not 1.
  //
  //   In practice, this function should never return false.
  //   Caution : not all aspects of the IEEE standard are
  //   checked by this function. For example, the IEEE standard
  //   requires particular features of the elementary operators,
  //   the management of nans, infs, etc... None of these
  //   properties are checked.
  //
  // Examples
  // // The following should return %t
  // isieee = flps_isIEEE ( )
  //
  // Authors
  //   Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_isIEEE" , rhs , 0 )
  apifun_checklhs ( "flps_isIEEE" , lhs , 1 )
  //
  f1 = flps_systemnew("current")
  f2 = flps_systemnew ( "IEEEdouble" )
  isieee = and( f1 == f2 )
endfunction

