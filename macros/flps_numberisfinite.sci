// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function is = flps_numberisfinite ( flpn )
  // Returns true if the number is finite.
  //
  // Calling Sequence
  //   is = flps_numberisfinite ( flpn )
  //
  // Parameters
  //   flpn : a floating point number
  //   is : a 1x1 matrix of boolean
  //
  // Description
  //   Returns true if the number is a finite.
  //   A finite number is not a nan and not an infinite.
  //   A finite number may be either zero, normal or subnormal.
  //
  // Examples
  // flps = flps_systemnew ( "IEEEdouble" );
  // // Get a finite number
  // flpn = flps_numbernew ( "double" , flps , 1 );
  // is = flps_numberisfinite ( flpn ) // %t
  // flpn = flps_numbernew ( "double" , flps , %nan );
  // is = flps_numberisfinite ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , +0 );
  // is = flps_numberisfinite ( flpn ) // %t
  // flpn = flps_numbernew ( "double" , flps , -0 );
  // is = flps_numberisfinite ( flpn ) // %t
  // flpn = flps_numbernew ( "double" , flps , 1 );
  // is = flps_numberisfinite ( flpn ) // %t
  // flpn = flps_numbernew ( "double" , flps , -1 );
  // is = flps_numberisfinite ( flpn ) // %t
  //
  // // Get others
  // flpn = flps_numbernew ( "double" , flps , -%inf );
  // is = flps_numberisfinite ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , %inf );
  // is = flps_numberisfinite ( flpn ) // %f
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  is = ( ~flps_numberisinf ( flpn ) & ~flps_numberisnan ( flpn ) )
endfunction

