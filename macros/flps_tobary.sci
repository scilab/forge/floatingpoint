// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function d = flps_tobary ( varargin )
  //  Returns the digits of the b-ary decomposition.
  //
  // Calling Sequence
  // d = flps_tobary ( x )
  // d = flps_tobary ( x , b )
  // d = flps_tobary ( x , b , p )
  //
  // Parameters
  //   x : 1x1 matrix of doubles, the floating point number to decompose
  //   b : a 1x1 matrix of floating point integers, the radix (default b=2)
  //   p : a 1x1 matrix of floating point integers, the precision (default p=53)
  //   d : a p-by-1 matrix, the digits of the b-ary decomposition of x.
  //
  // Description
  //   Returns the digits of the b-ary decomposition of x in base b with
  //   precision p.
  //   Given a number x and a radix b such that
  //   0 <= x < b, and given a precision p,
  //   returns the digits d(i), for i = 1, 2, ..., p,
  //   with x = d(1) + d(2)/b + d(3)/b^2 + ... + d(p-1)/b^(b-1).
  //
  // Any optional input argument equal to the empty matrix is replaced by its default value.
  //
  //   Caution : there is no specific rounding mode guaranteed
  //   by this function, which implies that the last digit
  //   is most of the times unsafe.
  //
  // Examples
  //   // This is d = [1 0]
  //   d = flps_tobary ( 1 , 2 , 2 )
  //   // This is d = [0 1 0 1]
  //   x = 1/3+1/3^3
  //   d = flps_tobary ( x , 3 , 4 )
  //   // This is d = [1 1 0 1 0 1]
  //   x = 1 + 1/2 + 1/2^3 + 1/2^5
  //   d = flps_tobary ( x , 2 , 6 )
  //
  // Authors
  // Michael Baudin, 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_tobary" , rhs , 1:3 )
  apifun_checklhs ( "flps_tobary" , lhs , 1 )
  //
  x = varargin(1)
  b = argindefault ( rhs , varargin , 2 , 2 )
  p = argindefault ( rhs , varargin , 3 , 53 )
  //
  apifun_checktype ( "flps_tobary" , x , "x" , 1 , "constant" )
  apifun_checktype ( "flps_tobary" , b , "b" , 2 , "constant" )
  apifun_checktype ( "flps_tobary" , p , "p" , 3 , "constant" )
  //
  apifun_checkscalar ( "flps_tobary" , b , "b" , 2 )
  apifun_checkscalar ( "flps_tobary" , p , "p" , 3 )
  //
  apifun_checkgreq ( "flps_tobary" , b , "b" , 2 , 2 )
  apifun_checkgreq ( "flps_tobary" , p , "p" , 3 , 1 )
  //
  if ( x == [] ) then
    d = []
    return
  end
  apifun_checkscalar ( "flps_tobary" , x , "x" , 1 )
  apifun_checkrange ( "flps_tobary" , x , "x" , 1 , 0 , b )
  if ( x == b ) then
    errmsg = msprintf(gettext("%s: Unexpected x = %s. Expect x < %d."), "flps_tobary", string(x), b);
    error(errmsg)
  end
  //
  q = 1
  for i = 1 : p
    d(i) = floor(x/q)
    x = x - d(i) * q
    q = q/b
  end
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

