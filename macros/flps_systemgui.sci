// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function h = flps_systemgui ( varargin )
  //   Plots all the numbers in the current floating point system
  // Calling Sequence
  //  h = flps_systemgui ( flps )
  //  h = flps_systemgui ( flps , denormals )
  //  h = flps_systemgui ( flps , denormals , onlypos )
  //  h = flps_systemgui ( flps , denormals , onlypos , logscale )
  //
  // Parameters
  //   flps : a floating point system
  //   denormals : a 1x1 matrix of booleans. Set to true to include denormalized numbers. Set to false to ignore denormalized numbers. Default is %t.
  //   onlypos : a 1x1 matrix of booleans. Set to %t to include only positive numbers. Set to %f to include both positive and negative numbers. Default is %f.
  //   logscale : a 1x1 matrix of booleans. Set to %t to use a base-10 logarithmic scale. Set to %f to see the actual values. Default is %f.
  //
  // Description
  //   This function is used to see the distribution of floating point numbers.
  //
  // Any optional input argument equal to the empty matrix is replaced by its default value.
  //
  // Examples
  // // A toy system, including denormals and negative
  // radix = 2;
  // p = 3;
  // e = 3;
  // flps = flps_systemnew ( "format" , radix , p , e );
  // h = flps_systemgui ( flps );
  // // Wait : see the picture before closing it...
  // close(h);
  //
  // Authors
  // Michael Baudin, 2010
  //
  // Bibliography
  //   "Numerical Computing with MATLAB", Cleve Moler, chapter "Introduction to MATLAB", section 1.7 "Floating point arithmetic"
  //   "Accuracy and Stability of Numerical Algorithms", Nicolas Higham, SIAM, 2002
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_systemgui" , rhs , 1:4 )
  apifun_checklhs ( "flps_systemgui" , lhs , 1 )
  //
  flps = varargin(1)
  denormals = argindefault ( rhs , varargin , 2 , %t )
  onlypos = argindefault ( rhs , varargin , 3 , %f )
  logscale = argindefault ( rhs , varargin , 4 , %f )
  //
  apifun_checktype ( "flps_systemgui" , flps ,      "flps" ,      1 , "TFLSYS" )
  apifun_checktype ( "flps_systemgui" , denormals , "denormals" , 2 , "boolean" )
  apifun_checktype ( "flps_systemgui" , onlypos ,   "onlypos" ,   3 , "boolean" )
  apifun_checktype ( "flps_systemgui" , logscale ,   "logscale" ,   4 , "boolean" )
  //
  apifun_checkscalar ( "flps_systemgui" , denormals , "denormals" , 2 )
  apifun_checkscalar ( "flps_systemgui" , onlypos , "onlypos" , 3 )
  apifun_checkscalar ( "flps_systemgui" , logscale , "logscale" , 4 )
  //
  flpn = flps_systemall ( flps , denormals , onlypos );
  f = flps_numbereval ( flpn );
  if ( logscale == %t ) then
    // Remove zeros
    ii = find ( f == 0 )
    f(ii) = []
  end
  n = size(f,"r");
  drawlater();
  for i = 1 : n
    plot([f(i) f(i)],[-1 1],"b-")
  end
  xmin = 1.05 * min(f)
  xmax = 1.05 * max(f)
  h = gcf()
  h.children.data_bounds = [
    xmin -3
    xmax 3
  ];
  h.children.axes_visible= ["on" "off" "on"]
  if ( logscale == %t ) then
    h.children.log_flags = "lnn"
  end
  h.figure_size = [600 300]
  drawnow();
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

