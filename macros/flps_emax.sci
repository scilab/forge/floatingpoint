// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ emax , vmax ] = flps_emax ( radix , p )
  //   Returns the maximum exponent and value before overflow.
  //
  // Calling Sequence
  //   [ emax , vmax ] = flps_emax ( radix , p )
  //
  // Parameters
  //   radix: a 1x1 matrix of floating point integers, the basis
  //   p : a 1x1 matrix of floating point integers, the precision (number of digits in the significant)
  //   emax : a 1x1 matrix of floating point integers, maximum exponent before overflow
  //   vmax : a 1x1 matrix of floating point integers, maximum value before overflow
  //
  // Description
  //   This algorithm is based on a loop, with an
  //   increasing value t = radix * s. If t/radix is different
  //   from s, then the overflow threshold has been
  //   detected.
  //
  // Examples
  // [radix,rounding] = flps_radix ();
  // [eps,p] = flps_eps (radix);
  // [ emax , vmax ] = flps_emax ( radix , p )
  // number_properties( "huge" ) // Compare with vmax
  // number_properties( "maxexp" ) // Compare with emax
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Algorithms to Reveal Properties of Floating-Point Arithmetic", Michael A. Malcolm, Stanford University, Communications of the ACM, Volume 15,  Issue 11 (November 1972), Pages: 949 - 951
  //   "More on Algorithms that Reveal Properties of Floating Point Arithmetic Units", W. Morven Gentleman, University of Waterloo, Scott B. Marovich, Purdue University, Communications of the ACM, Volume 17,  Issue 5 (May 1974), Pages: 276 - 277
  //   "ALGORITHM 665 - MACHAR: A Subroutine to Dynamically Determine Machine Parameters", W. J. Cody, Argonne National Laboratory, ACM Transactions on Mathematical Software, Vol. 14, No. 4, December 1988, Pages 303-311.
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_emax" , rhs , 2 )
  apifun_checklhs ( "flps_emax" , lhs , 1:2 )
  //
  apifun_checktype ( "flps_emax" , radix , "radix" , 1 , "constant" )
  //
  apifun_checkscalar ( "flps_emax" , radix , "radix" , 1 )
  //
  apifun_checkgreq ( "flps_emax" , radix , "radix" , 1 , 0 )
  //
  vmax = 1.
  emax = 0
  while ( %t )
    emax = emax + 1
    // Condition #1 : no exception is generated
    t = vmax * radix
    // Condition #2 : one can recover the original number
    if ( t / radix <> vmax) then
      break
    end
    vmax = t
  end
  //
  // Our definition is
  //    x = M * beta ^ ( e - p + 1 )
  // where M is the integral significant in the interval [0,beta^p - 1[
  // and e is the exponent in the interval emin <= e <= emax.
  // Because of this definition, we must decrease emin by 1.
  // This does not change vmin.
  //
  emax = emax - 1
  //
  // Compute y = 1-radix^-p = 1-t^-p where t=1/radix
  // y = (1-t)*(1+t+t^2+t^3+...+t^(p-1)) = (1-t)*s
  // Then y = (1-t) + (1-t)t + (1-t)*t^2 + (1-t)*t^3 + ... + (1-t)*t^(p-1)
  // y = v + v*t + v*t^2 + v*t^3 + ... + v*t^(p-1) where v=1-t.
  // Avoid producing y > 1, as in BLAS/DLAMCH/DLAMC5.
  //
  t = 1/radix
  v = 1 - t
  z = v
  y = 0
  for k = 1 : p
    ynew = y + z
    if ( ynew > 1 ) then
      break
    end
    y = ynew
    z = t * z
  end
  //
  // Compute radix^emax
  //
  re = 1
  for k = 1 : emax
    re = radix * re
  end
  //
  // Compute vmax = (radix^p - 1) * radix^(emax-p+1) = radix * (1-radix^-p) * radix^emax = radix * y * re
  //
  vmax = radix * y * re
endfunction

