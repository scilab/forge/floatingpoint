// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = flps_frombary ( varargin )
  // Returns the floating point number given its b-ary decomposition.
  //
  // Calling Sequence
  // x = flps_frombary ( d )
  // x = flps_frombary ( d , b )
  //
  // Parameters
  //   d : a matrix of floating point integers, the digits in lower endian order.
  //   b : a 1x1 matrix of floating point integers, the radix (default b=2)
  //   x : a 1x1 matrix of doubles, the floating point number
  //
  // Description
  //   Given the radix b and the digits d(i), for i = 1, 2, ..., p,
  //   returns a number
  //   x = d(1) + d(2)/b + d(3)/b^2 + ... + d(p-1)/b^(b-1) such that
  //   0 <= x < b.
  //
  // Any optional input argument equal to the empty matrix is replaced by its default value.
  //
  // Examples
  // x = flps_frombary ( [1 0]' )
  // expected = 1
  // //
  // x = flps_frombary ( [1 0]' , 2 )
  // expected = 1
  // //
  // x = flps_frombary ( [0 1 0 1]' , 3 )
  // expected = 1/3+1/3^3
  // //
  // x = flps_frombary ( [1 1 0 1 0 1]' )
  // expected = 1 + 1/2 + 1/2^3 + 1/2^5
  //
  // Authors
  // Michael Baudin, 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_frombary" , rhs , 1:2 )
  apifun_checklhs ( "flps_frombary" , lhs , 1 )
  //
  d = varargin(1)
  b = argindefault ( rhs , varargin , 2 , 2 )
  //
  apifun_checktype ( "flps_frombary" , d , "d" , 1 , "constant" )
  apifun_checktype ( "flps_frombary" , b , "b" , 2 , "constant" )
  //
  apifun_checkscalar ( "flps_frombary" , b , "b" , 2 )
  //
  apifun_checkgreq ( "flps_frombary" , d , "d" , 1 , 0 )
  apifun_checkgreq ( "flps_frombary" , b , "b" , 2 , 2 )
  if ( find(b==primes(100))==[] ) then
    error(msprintf(gettext("%s: Basis %d is not prime."),"flps_frombary",b))
  end
  //
  x = 0
  q = 1
  p = size(d,"*")
  for i = 1 : p
    x = x + d(i) * q
    q = q/b
  end
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

