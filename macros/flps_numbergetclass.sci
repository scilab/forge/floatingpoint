// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function class = flps_numbergetclass ( flpn )
  // Returns the class of the number;
  //
  // Calling Sequence
  //   class = flps_numbergetclass ( flpn )
  //
  // Parameters
  //   flpn : a floating point number
  //   class : a 1x1 matrix of floating point integers. See below for details.
  //
  // Description
  //   Returns the class of floating point number.
  //   <itemizedlist>
  //   <listitem><para>class=1, flpn is a NaN</para></listitem>
  //   <listitem><para>class=2, flpn is an infinity</para></listitem>
  //   <listitem><para>class=3, flpn is a normal</para></listitem>
  //   <listitem><para>class=4, flpn is a subnormal</para></listitem>
  //   <listitem><para>class=5, flpn is a signed zero</para></listitem>
  //   </itemizedlist>
  //
  // Examples
  // flps = flps_systemnew ( "IEEEdouble" )
  // flpn = flps_numbernew ( "double" , flps , +0 )
  // class = flps_numbergetclass ( flpn ) // 5
  // flpn = flps_numbernew ( "double" , flps , -0 )
  // class = flps_numbergetclass ( flpn ) // 5
  // flpn = flps_numbernew ( "double" , flps , -%inf );
  // class = flps_numbergetclass ( flpn ) // 2
  // flpn = flps_numbernew ( "double" , flps , %inf );
  // class = flps_numbergetclass ( flpn ) // 2
  // flpn = flps_numbernew ( "double" , flps , %nan );
  // class = flps_numbergetclass ( flpn ) // 1
  // flpn = flps_numbernew ( "double" , flps , 1 );
  // class = flps_numbergetclass ( flpn ) // 3
  // flpn = flps_numbernew ( "double" , flps , -1 );
  // class = flps_numbergetclass ( flpn ) // 3
  // flpn = flps_numbernew ( "double" , flps , 5.e-320 )
  // class = flps_numbergetclass ( flpn ) // 4
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  if ( flps_numberisnan(flpn) ) then
    class = 1
  elseif ( flps_numberisinf(flpn) ) then
    class = 2
  elseif ( flps_numberisnormal(flpn) ) then
    class = 3
  elseif ( flps_numberissubnormal(flpn) ) then
    class = 4
  elseif ( flps_numberiszero(flpn) ) then
    class = 5
  else
    localstr = gettext("%s: Internal error: class unknown.")
    errmsg = msprintf(localstr,"flps_numbergetclass")
    error(errmsg)
  end
endfunction

