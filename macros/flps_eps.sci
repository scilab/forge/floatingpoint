// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ eps , p ] = flps_eps ( radix )
  //   Returns the machine epsilon and the precision for Scilab doubles.
  //
  // Calling Sequence
  //   eps = flps_eps ( radix )
  //   [ eps , p ] = flps_eps ( radix )
  //
  // Parameters
  //   radix: a 1x1 matrix of floating point integers, the basis
  //   eps : a 1x1 matrix of doubles, the smallest number for which 1+eps <> 1
  //   p : a 1x1 matrix of floating point integers, the precision (number of digits in the significant)
  //   rounding : a 1x1 matrix of booleans, true if round to nearest is used, false if not
  //
  // Description
  //   This algorithm is based on a loop, with an
  //   decreasing value in magnitude s = s/2. If 1+s==1, then
  //   2*s is the machine epsilon.
  //
  // Examples
  // [radix,rounding] = flps_radix ();
  // [eps,p] = flps_eps (radix)
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Algorithms to Reveal Properties of Floating-Point Arithmetic", Michael A. Malcolm, Stanford University, Communications of the ACM, Volume 15,  Issue 11 (November 1972), Pages: 949 - 951
  //   "More on Algorithms that Reveal Properties of Floating Point Arithmetic Units", W. Morven Gentleman, University of Waterloo, Scott B. Marovich, Purdue University, Communications of the ACM, Volume 17,  Issue 5 (May 1974), Pages: 276 - 277
  //   "ALGORITHM 665 - MACHAR: A Subroutine to Dynamically Determine Machine Parameters", W. J. Cody, Argonne National Laboratory, ACM Transactions on Mathematical Software, Vol. 14, No. 4, December 1988, Pages 303-311.
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_eps" , rhs , 1 )
  apifun_checklhs ( "flps_eps" , lhs , 1:2 )
  //
  apifun_checktype ( "flps_eps" , radix , "radix" , 1 , "constant" )
  //
  apifun_checkscalar ( "flps_eps" , radix , "radix" , 1 )
  //
  apifun_checkgreq ( "flps_eps" , radix , "radix" , 1 , 2 )
 //
  eps = 1.0;
  done = 0;
  p = 0;
  while ( done==0 )
    eps = eps / radix;
    if (1.0+eps==1.0) then
      done = 1;
    end
    p = p + 1;
  end
  eps = eps * radix;
endfunction

