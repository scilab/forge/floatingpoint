// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function M = flps_sme2M ( radix , p , s , m , e )
  //   Returns the integral significand from (s,m,e).
  //
  // Calling Sequence
  //   M = flps_sme2M ( radix , p , s , m , e )
  //
  // Parameters
  //   r : a 1x1 matrix of floating point integers, the radix
  //   p : a 1x1 matrix of floating point integers, the precision
  //   s : a 1x1 matrix of floating point integers, the sign
  //   m : a 1x1 matrix of floating point integers, the normal significand
  //   e : a 1x1 matrix of floating point integers, the exponent
  //   M : a 1x1 matrix of floating point integers, the integral significand
  //
  // Description
  //   Returns the integral significand M corresponding
  //   to the (s,m,e) floating point representation.
  //   The formula is
  //
  //   <latex>
  //   \begin{eqnarray}
  //   M = (-1)^s \cdot m \cdot \beta^{p-1},
  //   \end{eqnarray}
  //   </latex>
  //
  //   where beta is the radix.
  //
  // Examples
  // radix = 2
  // p = 53
  // s= 0
  // m= 1.9999999999999997779554
  // e= 0
  // expected= 9007199254740991
  // M = flps_sme2M ( radix , p , s , m , e )
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_Me2sm" , rhs , 5 )
  apifun_checklhs ( "flps_Me2sm" , lhs , 1 )
  //
  apifun_checktype ( "flps_Me2sm" , radix , "radix" , 1 , "constant" )
  apifun_checktype ( "flps_Me2sm" , p , "p" , 2 , "constant" )
  apifun_checktype ( "flps_Me2sm" , s , "s" , 3 , "constant" )
  apifun_checktype ( "flps_Me2sm" , m , "m" , 4 , "constant" )
  apifun_checktype ( "flps_Me2sm" , e , "e" , 5 , "constant" )
  //
  apifun_checkscalar ( "flps_Me2sm" , radix , "radix" , 1 )
  apifun_checkscalar ( "flps_Me2sm" , p ,     "p" ,     2 )
  apifun_checkscalar ( "flps_Me2sm" , s ,     "s" ,     3 )
  apifun_checkscalar ( "flps_Me2sm" , m ,     "m" ,     4 )
  apifun_checkscalar ( "flps_Me2sm" , e ,     "e" ,     5 )
  //
  apifun_checkgreq ( "flps_Me2sm" , radix , "radix" , 1 , 0 )
  apifun_checkgreq ( "flps_Me2sm" , p ,     "p" ,     2 , 0 )
  apifun_checkgreq ( "flps_Me2sm" , s ,     "s" ,     3 , 0 )
  apifun_checkgreq ( "flps_Me2sm" , m ,     "m" ,     4 , 0 )
  //
  M = (-1)^s * m * radix^(p - 1)
endfunction

