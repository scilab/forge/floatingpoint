// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

radix = 2;
p = 53;
//
s= 0;
m= 1.9999999999999997779554;
e= 0;
expected = 9007199254740991;
M = flps_sme2M ( radix , p , s , m , e );
assert_equal ( M , expected );
assert_equal ( flps_signbit(M) , %f );
//
// Test +0
s= 0;
m= 0;
e= 0;
expected = 0;
M = flps_sme2M ( radix , p , s , m , e );
assert_equal ( M , expected );
assert_equal ( flps_signbit(M) , %f );
//
// Test -0
s= 1;
m= 0;
e= 0;
expected = 0;
M = flps_sme2M ( radix , p , s , m , e );
assert_equal ( M , expected );
assert_equal ( flps_signbit(M) , %t );
//
// Test +1
s= 0;
m= 1;
e= 0;
expected = 4503599627370496;
M = flps_sme2M ( radix , p , s , m , e );
assert_equal ( M , expected );
assert_equal ( flps_signbit(M) , %f );
//
// Test -1
s= 1;
m= 1;
e= 0;
expected = -4503599627370496;
M = flps_sme2M ( radix , p , s , m , e );
assert_equal ( M , expected );
assert_equal ( flps_signbit(M) , %t );
//
// Test -2^-10
s= 1;
m= 1;
e= -10;
expected = -4503599627370496;
M = flps_sme2M ( radix , p , s , m , e );
assert_equal ( M , expected );
assert_equal ( flps_signbit(M) , %t );





