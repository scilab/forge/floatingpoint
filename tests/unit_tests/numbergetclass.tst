// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
expectedmatrix = [
+1      3
-1      3
+1.7976931348623157e+308 3 // largest normal
+2.2250738585072014e-308 3 // smallest normal
-1.7976931348623157e308  3 // largest negative normal
-2.2250738585072014e-308 3 // smallest negative normal
+0      5
-0      5
%inf    2
-%inf   2
%nan    1
-%nan   1
+2.2250738585072008e-308 4 // largest denormal
+4.9406564584124654e-324 4 // smallest denormal
-2.2250738585072008e-308 4 // largest negative denormal
-4.9406564584124654e-324 4 // smallest negative denormal
];

ntests = size(expectedmatrix,"r");
flps = flps_systemnew("IEEEdouble");
for k = 1 : ntests
  x = expectedmatrix(k,1);
  flpn = flps_numbernew ( "double" , flps , x );
  expected = expectedmatrix(k,2);
  class = flps_numbergetclass(flpn);
  assert_equal ( class , expected ) ;
end


