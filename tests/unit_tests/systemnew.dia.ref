// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->
//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
// Check the default constructor and the printer system
flps = flps_systemnew ()
 flps  =
Floating Point System:
======================
radix= []
p=     []
emin=  []
emax=  []
vmin=  []
vmax=  []
eps=   []
r=     []
gu=    []
alpha= []
ebits= []
assert_equal ( flps.radix , [] );
assert_equal ( flps.p , [] );
flps.radix = 2
 flps  =
Floating Point System:
======================
radix= 2
p=     []
emin=  []
emax=  []
vmin=  []
vmax=  []
eps=   []
r=     []
gu=    []
alpha= []
ebits= []
flps.p = 12
 flps  =
Floating Point System:
======================
radix= 2
p=     12
emin=  []
emax=  []
vmin=  []
vmax=  []
eps=   []
r=     []
gu=    []
alpha= []
ebits= []
// Get the current floating point system
flps = flps_systemnew("current");
assert_equal ( flps.radix , 2 );
flps
flps  =
Floating Point System:
======================
radix= 2
p=     53
emin=  -1022
emax=  1023
vmin=  2.23D-308
vmax=  1.80D+308
eps=   2.220D-16
r=     1
gu=    T
alpha= -2147483648.00D-324
ebits= 11
// Get empty current floating point system
flps = flps_systemnew("empty");
assert_equal ( flps.radix , [] );
assert_equal ( flps.p , [] );
flps
flps  =
Floating Point System:
======================
radix= []
p=     []
emin=  []
emax=  []
vmin=  []
vmax=  []
eps=   []
r=     []
gu=    []
alpha= []
ebits= []
//
flps = flps_systemnew("IEEEsingle");
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 24 );
assert_equal ( flps.ebits , 8 );
string(flps)
 ans  =
!Floating Point System:  !
!                        !
!======================  !
!                        !
!radix= 2                !
!                        !
!p=     24               !
!                        !
!emin=  -126             !
!                        !
!emax=  127              !
!                        !
!vmin=  1.175D-38        !
!                        !
!vmax=  3.403D+38        !
!                        !
!eps=   0.0000001        !
!                        !
!r=     1                !
!                        !
!gu=    T                !
!                        !
!alpha= 1.401D-45        !
!                        !
!ebits= 8                !
flps
flps  =
Floating Point System:
======================
radix= 2
p=     24
emin=  -126
emax=  127
vmin=  1.175D-38
vmax=  3.403D+38
eps=   0.0000001
r=     1
gu=    T
alpha= 1.401D-45
ebits= 8
//
flps = flps_systemnew("IEEEdouble");
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 53 );
assert_equal ( flps.ebits , 11 );
string(flps)
 ans  =
!Floating Point System:      !
!                            !
!======================      !
!                            !
!radix= 2                    !
!                            !
!p=     53                   !
!                            !
!emin=  -1022                !
!                            !
!emax=  1023                 !
!                            !
!vmin=  2.23D-308            !
!                            !
!vmax=  1.80D+308            !
!                            !
!eps=   2.220D-16            !
!                            !
!r=     1                    !
!                            !
!gu=    T                    !
!                            !
!alpha= -2147483648.00D-324  !
!                            !
!ebits= 11                   !
flps
flps  =
Floating Point System:
======================
radix= 2
p=     53
emin=  -1022
emax=  1023
vmin=  2.23D-308
vmax=  1.80D+308
eps=   2.220D-16
r=     1
gu=    T
alpha= -2147483648.00D-324
ebits= 11
//
flps = flps_systemnew("IEEEdoubleext");
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 64 );
assert_equal ( flps.ebits , 15 );
string(flps)
 ans  =
!Floating Point System:  !
!                        !
!======================  !
!                        !
!radix= 2                !
!                        !
!p=     64               !
!                        !
!emin=  -16382           !
!                        !
!emax=  16383            !
!                        !
!vmin=  0                !
!                        !
!vmax=  Inf              !
!                        !
!eps=   1.084D-19        !
!                        !
!r=     1                !
!                        !
!gu=    T                !
!                        !
!alpha= 0                !
!                        !
!ebits= 15               !
flps
flps  =
Floating Point System:
======================
radix= 2
p=     64
emin=  -16382
emax=  16383
vmin=  0
vmax=  Inf
eps=   1.084D-19
r=     1
gu=    T
alpha= 0
ebits= 15
//
// Test for double
flps = flps_systemnew ( "format" , 2 , 53 , 11 );
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 53 );
assert_equal ( flps.ebits , 11 );
assert_equal ( flps.eps , 2^-52 );
flps
flps  =
Floating Point System:
======================
radix= 2
p=     53
emin=  -1022
emax=  1023
vmin=  2.23D-308
vmax=  1.80D+308
eps=   2.220D-16
r=     1
gu=    T
alpha= -2147483648.00D-324
ebits= 11
//
// Check consistency between format database and format
//
flps1 = flps_systemnew("IEEEsingle");
flps2 = flps_systemnew ( "format" , 2 , 24 , 8 );
assert_equal ( and(flps1==flps2) , %t );
//
flps1 = flps_systemnew("IEEEdouble");
flps2 = flps_systemnew ( "format" , 2 , 53 , 11 );
assert_equal ( and(flps1==flps2) , %t );
//
flps1 = flps_systemnew("IEEEdoubleext");
flps2 = flps_systemnew ( "format" , 2 , 64 , 15 );
assert_equal ( and(flps1==flps2) , %t );
//
flps1 = flps_systemnew("IEEEdouble");
flps2 = flps_systemnew ( "current" );
assert_equal ( and(flps1==flps2) , %t );
//
