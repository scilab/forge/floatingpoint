// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

// A toy system, including denormals and negative
radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e );
h = flps_systemgui ( flps );
close(h);

// Without denormals
denormals = %f;
h = flps_systemgui ( flps , denormals );
close(h);

// Without denormals
// With positive only
onlypos = %t;
denormals = %f;
h = flps_systemgui ( flps , denormals , onlypos );
close(h);

// Without denormals
// With positive only
// In logscale
onlypos = %t;
denormals = %f;
logscale = %t;
h = flps_systemgui ( flps , denormals , onlypos , logscale );
close(h);

