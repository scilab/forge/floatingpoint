// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
    if ( and(isnan(computed)) & and(isnan(expected)) ) then
        flag = 1
    else
        if (computed==expected) then
            flag = 1;
        else
            flag = 0;
        end
    end
    if flag <> 1 then pause,end
endfunction

//
// Straitforward test
//
x = flps_hex2double ( "400921FB54442D18" );
assert_equal ( x , %pi ) ;

//
// Simple tests
//
path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv");
expectedmatrix = flps_datasetread ( dataset , "#" , "," , %f );
ntests = size(expectedmatrix,"r");
for k = 1 : ntests
  x = evstr(expectedmatrix(k,1));
  hex = stripblanks(expectedmatrix(k,6));
  mprintf("[IEEEdouble] Test #%d/%d, hex=%s > x=%s\n",k,ntests,hex,string(x));
  c = flps_hex2double(hex);
  assert_equal ( c , x ) ;
end


