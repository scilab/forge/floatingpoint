// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->
//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
expected = "400921FB54442D18";
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "double" , flps , %pi );
hexstr = flps_number2hex (flpn);
assert_equal ( hexstr , expected ) ;
//
// Grey-box: the function uses the format function
format("e",25)
expected = "400921FB54442D18";
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "double" , flps , %pi );
hexstr = flps_number2hex (flpn);
assert_equal ( hexstr , expected );
t = format();
assert_equal ( t , [0 25] );
format("v",10)
//
// Get and extract the binary string
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "double" , flps , %pi );
[hexstr,binstr] = flps_number2hex (flpn);
expected = "400921FB54442D18";
assert_equal ( hexstr , expected ) ;
expected = "0100000000001001001000011111101101010100010001000010110100011000";
assert_equal ( binstr , expected ) ;
//
sign_str = part(binstr,1);
expected = "0";
assert_equal ( sign_str , expected ) ;
//
expo_str = part(binstr,2:12);
expected = "10000000000";
assert_equal ( expo_str , expected ) ;
//
M_str = part(binstr,13:64);
expected = "1001001000011111101101010100010001000010110100011000";
assert_equal ( M_str , expected ) ;
//
// Tests for doubles
path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv");
expectedmatrix = flps_datasetread ( dataset , "#" , "," , %f );
ntests = size(expectedmatrix,"r");
flps = flps_systemnew("IEEEdouble");
for k = 1 : ntests
  x = evstr(expectedmatrix(k,1));
  hex = stripblanks(expectedmatrix(k,6));
  mprintf("[IEEEdouble] Test #%d/%d,x=%s\n",k,ntests,string(x));
  //
  flpn = flps_numbernew ( "double" , flps , x );
  hexstr = flps_number2hex(flpn);
  assert_equal ( hexstr , hex ) ;
end
[IEEEdouble] Test #1/37,x=2048
[IEEEdouble] Test #2/37,x=-2048
[IEEEdouble] Test #3/37,x=1024
[IEEEdouble] Test #4/37,x=-1024
[IEEEdouble] Test #5/37,x=0
[IEEEdouble] Test #6/37,x=0
[IEEEdouble] Test #7/37,x=Nan
[IEEEdouble] Test #8/37,x=Inf
[IEEEdouble] Test #9/37,x=-Inf
[IEEEdouble] Test #10/37,x=1.80D+308
[IEEEdouble] Test #11/37,x=-1.80D+308
[IEEEdouble] Test #12/37,x=2.23D-308
[IEEEdouble] Test #13/37,x=-2.23D-308
[IEEEdouble] Test #14/37,x=2.23D-308
[IEEEdouble] Test #15/37,x=-2.23D-308
[IEEEdouble] Test #16/37,x=-2147483648.00D-324
[IEEEdouble] Test #17/37,x=--2147483648.00D-324
[IEEEdouble] Test #18/37,x=3.1415927
[IEEEdouble] Test #19/37,x=-3.1415927
[IEEEdouble] Test #20/37,x=0.3333333
[IEEEdouble] Test #21/37,x=-0.3333333
[IEEEdouble] Test #22/37,x=31415.927
[IEEEdouble] Test #23/37,x=-31415.927
[IEEEdouble] Test #24/37,x=93648.047
[IEEEdouble] Test #25/37,x=-93648.047
[IEEEdouble] Test #26/37,x=3.14D+200
[IEEEdouble] Test #27/37,x=-3.14D+200
[IEEEdouble] Test #28/37,x=3.14D-200
[IEEEdouble] Test #29/37,x=-3.14D-200
[IEEEdouble] Test #30/37,x=2
[IEEEdouble] Test #31/37,x=-2
[IEEEdouble] Test #32/37,x=1
[IEEEdouble] Test #33/37,x=-1
[IEEEdouble] Test #34/37,x=5.00D-323
[IEEEdouble] Test #35/37,x=-5.00D-323
[IEEEdouble] Test #36/37,x=10.02D-321
[IEEEdouble] Test #37/37,x=-10.02D-321
//
// Tests for singles
//
// [M e hex]
path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEsingle.csv");
expectedmatrix = flps_datasetread ( dataset , "#" , "," , %f );
ntests = size(expectedmatrix,"r");
flps = flps_systemnew("IEEEsingle");
for k = 1 : ntests
  x = evstr(expectedmatrix(k,1));
  hex = stripblanks(expectedmatrix(k,6));
  mprintf("[IEEEsingle] Test #%d/%d,x=%s\n",k,ntests,string(x));
  //
  flpn = flps_numbernew ( "double" , flps , x );
  hexstr = flps_number2hex(flpn);
  assert_equal ( hexstr , hex ) ;
end
[IEEEsingle] Test #1/23,x=1
[IEEEsingle] Test #2/23,x=-1
[IEEEsingle] Test #3/23,x=2
[IEEEsingle] Test #4/23,x=-2
[IEEEsingle] Test #5/23,x=0.3333333
[IEEEsingle] Test #6/23,x=8.816D-39
[IEEEsingle] Test #7/23,x=2047.9999
[IEEEsingle] Test #8/23,x=-2047.9999
[IEEEsingle] Test #9/23,x=1024
[IEEEsingle] Test #10/23,x=-1024
[IEEEsingle] Test #11/23,x=0
[IEEEsingle] Test #12/23,x=0
[IEEEsingle] Test #13/23,x=Nan
[IEEEsingle] Test #14/23,x=Inf
[IEEEsingle] Test #15/23,x=-Inf
[IEEEsingle] Test #16/23,x=3.403D+38
[IEEEsingle] Test #17/23,x=-3.403D+38
[IEEEsingle] Test #18/23,x=1.175D-38
[IEEEsingle] Test #19/23,x=-1.175D-38
[IEEEsingle] Test #20/23,x=1.175D-38
[IEEEsingle] Test #21/23,x=-1.175D-38
[IEEEsingle] Test #22/23,x=1.401D-45
[IEEEsingle] Test #23/23,x=-1.401D-45
