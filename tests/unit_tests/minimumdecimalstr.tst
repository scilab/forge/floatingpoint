// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
computed = flps_minimumdecimalstr ( "1.1000000000000000888178" );
assert_equal ( computed , "1.1" );
//
instr = "computed = flps_minimumdecimalstr ( [""faa"",""1.1""] )";
ierr = execstr(instr,"errcatch");
laerr = lasterror();
assert_equal ( ierr , 10000 );
assert_equal ( laerr , "flps_minimumdecimalstr: Expected only numbers in input argument #1, but variable xstr contains also a non-number at entry #1: faa." );
//
xstr = ["1.1000000000000000888178" "1.2345678912345678"];
computed = flps_minimumdecimalstr ( xstr );
expected = ["1.1" "1.2345678912345678"];
assert_equal ( computed , expected );
//
computed = flps_minimumdecimalstr ( "1.e10" );
assert_equal ( computed , "1.e10" );
//
computed = flps_minimumdecimalstr ( "1.1000000000000000888178e10" );
assert_equal ( computed , "1.1e10" );
//
computed = flps_minimumdecimalstr ( "1.e010" );
assert_equal ( computed , "1.e10" );
//
computed = flps_minimumdecimalstr ( "1.1000000000000000888178e010" );
assert_equal ( computed , "1.1e10" );
//
computed = flps_minimumdecimalstr ( "1.1000000000000000888178e+010" );
assert_equal ( computed , "1.1e+10" );
//
computed = flps_minimumdecimalstr ( "1.1000000000000000888178e-010" );
assert_equal ( computed , "1.10000000000000008e-10" );
//
computed = flps_minimumdecimalstr ( "1.1000000000000000888178e+010" );
assert_equal ( computed , "1.1e+10" );
//
computed = flps_minimumdecimalstr ( "1.1000000000000000888178e-10" );
assert_equal ( computed , "1.10000000000000008e-10" );
//
assert_equal ( flps_minimumdecimalstr ( "+0" ) , "+0" );
//
assert_equal ( flps_minimumdecimalstr ( "-0" ) , "-0" );
//
assert_equal ( flps_minimumdecimalstr ( "+0.00000000000" ) , "+0." );
//
assert_equal ( flps_minimumdecimalstr ( "-0.00000000000" ) , "-0." );
//
assert_equal ( flps_minimumdecimalstr ( "0000000.00000000000" ) , "0." );
//
assert_equal ( flps_minimumdecimalstr ( "0000000.00000001" ) , ".00000001" );
//
assert_equal ( flps_minimumdecimalstr ( "2.00047180001120725e-320" ) , "2.0004e-320" );

