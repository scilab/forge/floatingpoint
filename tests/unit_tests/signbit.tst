// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

assert_equal ( flps_signbit ( 1 ) , %f );
assert_equal ( flps_signbit ( -1 ) , %t );
assert_equal ( flps_signbit ( 0 ) , %f );
assert_equal ( flps_signbit ( -0 ) , %t );
//
x = [
  1 2 3
  -1 -2 -3
];
b = flps_signbit ( x );
expected = [
  %f %f %f
  %t %t %t
];
assert_equal ( b , expected );
//
x =        [+0 -0 %inf -%inf %nan -%nan];
expected = [%f %t %f   %t    %f   %f];
b = flps_signbit ( x );
assert_equal ( and(b==expected) , %t );

