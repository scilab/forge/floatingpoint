// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

// Check the default constructor and the printer system
flps = flps_systemnew ()
assert_equal ( flps.radix , [] );
assert_equal ( flps.p , [] );
flps.radix = 2
flps.p = 12

// Get the current floating point system
flps = flps_systemnew("current");
assert_equal ( flps.radix , 2 );
flps

// Get empty current floating point system
flps = flps_systemnew("empty");
assert_equal ( flps.radix , [] );
assert_equal ( flps.p , [] );
flps
//
flps = flps_systemnew("IEEEsingle");
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 24 );
assert_equal ( flps.ebits , 8 );
string(flps)
flps
//
flps = flps_systemnew("IEEEdouble");
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 53 );
assert_equal ( flps.ebits , 11 );
string(flps)
flps
//
flps = flps_systemnew("IEEEdoubleext");
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 64 );
assert_equal ( flps.ebits , 15 );
string(flps)
flps
//
// Test for double
flps = flps_systemnew ( "format" , 2 , 53 , 11 );
assert_equal ( flps.radix , 2 );
assert_equal ( flps.p , 53 );
assert_equal ( flps.ebits , 11 );
assert_equal ( flps.eps , 2^-52 );
flps
//
// Check consistency between format database and format
//
flps1 = flps_systemnew("IEEEsingle");
flps2 = flps_systemnew ( "format" , 2 , 24 , 8 );
assert_equal ( and(flps1==flps2) , %t );
//
flps1 = flps_systemnew("IEEEdouble");
flps2 = flps_systemnew ( "format" , 2 , 53 , 11 );
assert_equal ( and(flps1==flps2) , %t );
//
flps1 = flps_systemnew("IEEEdoubleext");
flps2 = flps_systemnew ( "format" , 2 , 64 , 15 );
assert_equal ( and(flps1==flps2) , %t );
//
flps1 = flps_systemnew("IEEEdouble");
flps2 = flps_systemnew ( "current" );
assert_equal ( and(flps1==flps2) , %t );
//

