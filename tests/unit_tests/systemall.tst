// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

// A toy system, including denormals and negative
radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e );
listflpn = flps_systemall ( flps );
n = size(listflpn);
x = flps_numbereval ( listflpn );
nx = 56;
assert_equal ( n , nx );
e = [
-14.       -12.       -10.       -8.        -7.        -6.        -5.        -4.
-3.5       -3.        -2.5       -2.        -1.75      -1.5       -1.25      -1.
-0.875     -0.75      -0.625     -0.5       -0.4375    -0.375     -0.3125    -0.25
-0.1875    -0.125     -0.0625     0.         0.         0.0625     0.125      0.1875
 0.25       0.3125     0.375      0.4375     0.5        0.625      0.75       0.875
 1.         1.25       1.5        1.75       2.         2.5        3.         3.5
 4.         5.         6.         7.         8.         10.        12.        14.
];
e = matrix(e',nx,1);
assert_equal ( x , e );
sb = flps_signbit(x);
expected = [];
expected(1:nx/2,1) = (ones(nx/2,1)==ones(nx/2,1));
expected(nx/2+1:nx,1) = (ones(nx/2,1)<>ones(nx/2,1));
assert_equal ( sb , expected );
//
// A toy system, excluding denormals and including negative
radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e );
denormals = %f;
listflpn = flps_systemall ( flps , denormals );
n = size(listflpn);
x = flps_numbereval ( listflpn );
nx = 50;
assert_equal ( n , nx );
e = [
-14.       -12.       -10.       -8.        -7.        -6.        -5.        -4.        -3.5       -3.
-2.5       -2.        -1.75      -1.5       -1.25      -1.        -0.875     -0.75      -0.625     -0.5
-0.4375    -0.375     -0.3125    -0.25       0.         0.         0.25       0.3125     0.375      0.4375
 0.5        0.625      0.75       0.875      1.         1.25       1.5        1.75       2.         2.5
 3.         3.5        4.         5.         6.         7.         8.         10.        12.        14.
];
e = matrix(e',nx,1);
assert_equal ( x , e );
sb = flps_signbit(x);
expected = [];
expected(1:nx/2,1) = (ones(nx/2,1)==ones(nx/2,1));
expected(nx/2+1:nx,1) = (ones(nx/2,1)<>ones(nx/2,1));
assert_equal ( sb , expected );

// A toy system, excluding denormals and excluding negative
radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e );
denormals = %f;
onlypos = %t;
listflpn = flps_systemall ( flps , denormals , onlypos );
n = size(listflpn);
x = flps_numbereval ( listflpn );
nx = 25;
assert_equal ( n , nx );
e = [
 0.         0.25       0.3125     0.375      0.4375
 0.5        0.625      0.75       0.875      1.
 1.25       1.5        1.75       2.         2.5
 3.         3.5        4.         5.         6.
 7.         8.         10.        12.        14.
];
e = matrix(e',nx,1);
assert_equal ( x , e );
sb = flps_signbit(x);
expected = [];
expected(1:nx,1) = (ones(nx,1)<>ones(nx,1));
assert_equal ( sb , expected );
//
// A toy system, including denormals and excluding negative
radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e );
denormals = %t;
onlypos = %t;
listflpn = flps_systemall ( flps , denormals , onlypos );
n = size(listflpn);
x = flps_numbereval ( listflpn );
nx = 28;
assert_equal ( n , nx );
e = [
    0.          0.0625      0.125       0.1875      0.25        0.3125      0.375
    0.4375      0.5         0.625       0.75        0.875       1.          1.25
    1.5         1.75        2.          2.5         3.          3.5         4.
    5.          6.          7.          8.          10.         12.         14.
];
e = matrix(e',nx,1);
assert_equal ( x , e );
sb = flps_signbit(x);
expected = [];
expected(1:nx,1) = (ones(nx,1)<>ones(nx,1));
assert_equal ( sb , expected );

